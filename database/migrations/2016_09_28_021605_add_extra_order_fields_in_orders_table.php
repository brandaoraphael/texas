<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraOrderFieldsInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders',function(Blueprint $table){

            $table->integer('user_address_id')->unsigned()->after('user_id');
            $table->smallInteger('payment_mode')->unsigned()->after('user_address_id');
            $table->float('delivery_fee')->default(0)->after('value');
            $table->float('total')->after('value');
            $table->string('change')->nullable()->after('total');
            $table->string('obs')->nullable()->after('change');
            $table->foreign('user_address_id')->references('id')->on('user_addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders',function(Blueprint $table){

            $table->dropColumn('user_address_id');
            $table->dropColumn('payment_mode');
            $table->dropColumn('delivery_fee');
            $table->dropColumn('total');
            $table->dropColumn('change');
        });
    }
}

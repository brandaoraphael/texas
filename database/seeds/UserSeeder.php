<?php

use Illuminate\Database\Seeder;
use App\Admin;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::updateOrCreate(

            ['username' => 'admin'],

            [
                'name'          => 'Master Admin',
                'email'         => 'admin@texas',
                'username'      => 'admin',
                'password'      => Hash::make('123456'),
                'role'          => 0,
            ]
        );
    }
}

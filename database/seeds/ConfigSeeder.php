<?php

use Illuminate\Database\Seeder;
use App\Config;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Config::updateOrCreate(

            ['key' => 'delivery_fee'],

            [
                'name'          => 'Valor da taxa',
                'value'         => '0',
                'placeholder'   => 'Inserir valor da taxa de entrega',
            ]
        );

        Config::updateOrCreate(

            ['key' => 'minimum_for_free_delivery'],

            [
                'name'          => 'Valor do pedido para entrega grátis',
                'value'         => '0',
                'placeholder'   => 'Insira o valor mínimo para entrega grátis'
            ]
        );

        Config::updateOrCreate(

            ['key' => 'packing_fee'],

            [
                'name'          => 'Valor para base de cálculo de embalagem',
                'value'         => '0',
                'placeholder'   => 'Insira o valor para base de cálculo de embalagem'
            ]
        );

        Config::updateOrCreate(

            ['key' => 'packing_price'],

            [
                'name'          => 'Valor da embalagem',
                'value'         => '0',
                'placeholder'   => 'Insira o valor da embalagem'
            ]
        );
    }
}

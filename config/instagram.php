<?php

/*
 * This file is part of Laravel Instagram.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | Instagram Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main' => [
            'id' => 'e215e7cb00b54ff0b9abec338ef4cda0',
            'secret' => '0a940ad1c79a43c5adc1243b9936fd1e',
            'access_token' => '{ "access_token": "1508209475.e215e7c.5cc0a07a2fac41c3b6394e925523b41d"}'
        ],

//        'alternative' => [
//            'id' => '382b7aca3ff549c289d7a27c13d82b28',
//            'secret' => '892e05da0a0f4f97943555016cbab224',
//            'access_token' => '4106882783.382b7ac.e91c01b3aab647138fdc5a01519959be'
//        ],

    ],

];

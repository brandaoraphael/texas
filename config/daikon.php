<?php

return [

    'dias_funcionamento' => [1,2,3,4,5,6,7],

    'hora_funcionamento' => [

        'inicio' => '00:00',
        'fim' => '23:59'
    ],

    'dados_empresa' => [

        'url'           => 'daikonrestaurante.com.br',
        'nome'          => 'DAIKON RESTAURANTE JAPONES',
        'razao'         => 'DAIKON RESTAURANTE LTDA',
        'endereco'      => 'CAPITAO JOAO ALVES DE LIRA',
        'numero'        => '303',
        'complemento'   => '',
        'bairro'        => 'PRATA',
        'cidade'        => 'CAMPINA GRANDE',
        'estado'        => 'PARAIBA',
        'cep'           => '58400-560'
    ],

    'payment_options'   => [

        'ta'                => 0.17,
        'base_ta'           => 0.3471
    ],

    'payment_modes' => [

        0   => 'Dinheiro',
        1   => 'Cartão - Débito (Visa)',
        2   => 'Cartão - Crédito (Visa)',
        3   => 'Cartão - Débito (Master)',
        4   => 'Cartão - Crédito (Master)',
        5   => 'Cartão - Débito (Elo)',
        6   => 'Cartão - Crédito (Elo)',
        7   => 'Cartão - Crédito (Hiper)',
	8   => 'Cartão - Crédito (American Express)',
	9   => 'Cartão - Crédito (Diners Club)',        
	10  => 'Cartão - Crédito (FortBrasil)',
        11  => 'Alelo Refeição',
        12  => 'Ticket Refeição',
    ],

    'paginate' => 30
];

<!DOCTYPE html>
<html lang="en">
    <head>
        @include('includes._head')
    </head>
    <body>
        @if(request()->route()->getName() == 'index')
            @include('includes._header_home')
        @else
            @include('includes._header')
        @endif
        @yield('content')
        @include('includes._footer')
    </body>
</html>
@extends('layouts.master')
@section('content')


    <div id="inicio" class="headerfix">
        <section>
            <div class="banner">
                <img class="wow fadeInUp" data-wow-delay='1.3s' src="{{ asset('assets/img/img-home.png') }}" alt="">
                <div class="banner-content wow fadeInLeft" data-wow-delay='1.5s'>
                    <h1>Bateu a fome?</h1>
                    <h2>Chama o <span class="display-block">Xerife!</span></h2>

                    <a href="{{ route('product.index') }}" target="_blank" class="button display-block" data-wow-delay='2s'>
                        <span><i>★</i><b>★</b><i>★</i></span>
                        Peça agora pelo site
                        <span><i>★</i><strong>★</strong><i>★</i></span>
                    </a>
                </div>
            </div>
            <img class="bg-home wow fadeInCustom" data-wow-duration='4s' src="{{ asset('assets/img/bg-home.jpg') }}" alt="">
        </section>
    </div>

    <div id="destaques" class="headerfix">
        <section>
            <ul class="wrap">
                <li class="wow fadeInUp">
                    <i class="pe-is-f-burger-2"></i>
                    <h3><strong>Hamburgeres</strong> Artesanais</h3>
                    <p>Escolher os melhores cortes e consequentemente o melhor Blend de carne transforma o misto entre fibra e gordura no sabor indiscutível de um bom hambúrguer Texano, depois basta cuidar bem da chapa e pronto, bem-vindo ao Texas Hamburgueria Artesanal.</p>
                </li>
                <li class="wow fadeInDown" data-wow-delay="0.15s">
                    <i class="pe-is-f-can-3"></i>
                    <h3><strong>Cervejas</strong>Importadas</h3>
                    <p>É indiscutível o fato de que cerveja e hambúrguer pode ser considerada para alguns uma combinação inseparável, portanto, o Texas tratou de selecionar as melhores cervejas do mercado, pra trazer para você, cliente, uma experiência interessante, após escolher um bom hambúrguer, escolha também uma boa cerveja.</p>
                </li>
                <li class="wow fadeInUp" data-wow-delay="0.3s">
                    <i class="pe-is-f-flatware-3"></i>
                    <h3><strong>Costela Suína</strong> ao Barbecue</h3>
                    <p>Um carro-chefe do Texas, Costela Suína bem grelhada e combinada com um delicioso molho Barbecue, uma carne macia e que desmancha na boca é o que tem feito da Texas Ribs e da Double Texas Ribs excelentes pedidas para comer acompanhado.</p>
                </li>
            </ul>

            <div class="wrap row padding-small-left padding-small-right">
                <div class="col-7 text-center padding-default-top wow fadeInUp" data-wow-duration="1.5s">
                    <h3 class="section-title margin-default-top margin-smartphone-zero-top">
                        <span>★<b>★</b>★</span>
                        Ingredientes Selecionados
                    </h3>
                    <h4 class="section-subtitle">O diferencial da nossa casa!</h4>
                    <p class="section-content border-default">
                        Trabalhar com ingredientes frescos e selecionados é uma tarefa árdua para qualquer área do setor alimentício, mas é uma regra para quem quer ver os clientes retornando. Começando pela seleção das carnes que serão utilizadas, pão novinho e fabricado especialmente para o Texas, os melhores queijos do mercado, saladas sempre fresquinhas e molhos preparados pela casa, não há segredos escondidos à sete chaves o que há é um olhar cuidadoso e o desejo de fazer sempre melhor.
                    </p>
                </div>
                <div class="col-5 wow fadeInLeft display-smartphone-block text-center" data-wow-duration="1.5s">
                    <img src="{{ asset('assets/img/img-prato01.png') }}" alt="">
                </div>
            </div>
        </section>
    </div>

    <div id="sobre" class="headerfix">
        <section>
            <div class="wrap row padding-small-left padding-small-right">
                <div class="col-4 wow zoomIn">
                    <ul class="gallery">
                        <li class="large">
                            <img src="{{ asset('assets/img/img-sobre01.jpg') }}" alt="">
                        </li>
                        <li>
                            <img src="{{ asset('assets/img/img-sobre02.jpg') }}" alt="">
                        </li>
                        <li>
                            <img src="{{ asset('assets/img/img-sobre03.jpg') }}" alt="">
                        </li>
                        <li>
                            <img src="{{ asset('assets/img/img-sobre03.jpg') }}" alt="">
                        </li>
                    </ul>
                </div>
                <div class="col-8 padding-small-left padding-smartphone-zero text-center wow fadeInUp" data-wow-duration="1.4s">
                    <h3 class="section-title">
                        <span>★<b>★</b>★</span>
                        A história do Texas
                    </h3>
                    <p class="section-content">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, explicabo velit aspernatur libero inventore, natus dignissimos incidunt aliquam enim distinctio molestiae, beatae. Maiores corporis iusto deleniti ducimus rerum adipisci rem? Quisquam non rerum illum similique, atque nemo amet tempore fuga a perferendis odit sapiente ab nisi assumenda magnam, corporis aut.
                    </p>
                    <p class="section-content">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia illo, ipsa autem non laboriosam, tempora nesciunt aspernatur est odit aliquam repudiandae reprehenderit! Laboriosam, asperiores. Harum illum enim, qui repellat amet!
                    </p>
                    <p class="section-content">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia illo, ipsa autem non laboriosam, tempora nesciunt aspernatur est odit aliquam repudiandae reprehenderit! Laboriosam, asperiores. Harum illum enim, qui repellat amet!
                    </p>
                </div>
            </div>
        </section>
    </div>

    <div id="delivery" class="headerfix">
        <section>
            <div class="wrap">
                <div class="text-center">
                    <h3 class="section-title wow fadeInUp">
                        <span>★<b>★</b>★</span>
                        Delivery
                    </h3>
                    <h4 class="section-subtitle wow fadeInUp">Confira os mais pedidos</h4>
                    <p class="section-content wow fadeInUp">
                       Se você está pedindo pela primeira vez nós facilitaremos, abaixo estão os mais desejados da casa.
                    </p>

                    <ul class="products row wow fadeInDown">
                        <li class="col-4 padding-small">
                            <div class="img">
                                <img src="{{ asset('assets/img/img-destaque03.png') }}" alt="">
                            </div>
                            <div class="product-title">
                                <strong>Austin</strong>
                                Se você curte um bom hambúrguer de frango então vai se amarrar no nosso Austin, a clássica combinação entre frango e o verdadeiro catupiry é também uma excelente opção pra quem não curte carne bovina mas deseja provar um de nossos hambúrgueres.
                            </div>
                        </li>
                        <li class="col-4 padding-small">
                            <div class="img">
                                <img src="{{ asset('assets/img/img-destaque01.png') }}" alt="">
                            </div>
                            <div class="product-title">
                                <strong>Monster Bacon</strong>
                                Você ama bacon? Tipo muito bacon? Conheça o nosso monstro que leva carne bovina com bacon (200g) enrolada em fatias de bacon, 3 fatias de cheddar e molho de bacon com crispy bacon.
                            </div>
                        </li>
                        <li class="col-4 padding-small">
                            <div class="img">
                                <img src="{{ asset('assets/img/img-destaque02.png') }}" alt="">
                            </div>
                            <div class="product-title">
                                <strong>Texas Burger</strong>
                                O tradicional hambúrguer com molho Barbecue é um dos mais pedidos e não é à toa que leva o nosso nome e leva um hambúrguer de Picanha (200g), 2x cheddar, molho Barbecue, picles e alface.
                            </div>
                        </li>
                    </ul>
                    <div class="padding-default-top wow fadeIn">
                        <a href="{{ route('product.index') }}" target="_blank" class="button display-inline-block">Ver o cardápio completo</a>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="contato" class="headerfix">
        <section>
            <div class="wrap padding-small-right padding-small-left">
                <div class="row text-center wow fadeInUp">
                    <div class="col-6 display-smartphone-block">
                        <div class="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.2286980167905!2d-35.88412518610895!3d-7.214732772837936!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ac1fad52c7426d%3A0xf000cd3c08c1a653!2sTexas+Hamburgueria+Artesanal!5e0!3m2!1spt-BR!2sbr!4v1476434742651" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class="col-6 infos display-smartphone-block">
                        <h3 class="section-title wow fadeInUp text-center">
                            <span>★<b>★</b>★</span>
                            Contate-nos
                        </h3>
                        <h4 class="section-subtitle padding-small-bottom wow fadeInUp text-center">Saiba como e onde nos encontrar</h4>
                        <ul class="text-left location">
                            <li>
                                <i class="ti-location-pin"></i>
                                R. Janúncio Ferreira, 192 - Campina Grande, PB<br/>
                                <a href="">Exibir no mapa</a>
                            </li>
                            <li>
                                <i class="ti-email"></i>
                                <a href="mailto:texasburgercg@gmail.com">texasburgercg@gmail.com</a>
                            </li>
                            <li>
                                <i class="ti-headphone-alt"></i>
                                (83) 3341-6153
                            </li>
                        </ul>
                        <ul class="text-left location">
                            <li>
                                <i class="ti-facebook"></i>
                                <a href="https://facebook.com/TexasHA" target="_blank">facebook.com/TexasHA</a>
                            </li>
                            <li>
                                <i class="ti-instagram"></i>
                                <a href="https://instagram.com/texas_hamburgueria" target="_blank">@texas_hamburgueria</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="insta" class="headerfix">
        <section>
            <div class="wrap">
                <div class="text-center row">
                    <h3 class="section-title wow fadeInUp">
                        <span>★<b>★</b>★</span>
                        Instagram
                    </h3>
                    <p class="section-content wow fadeInUp">
                        Confira tudo o que está rolando no Instagram oficial do Texas.
                    </p>
                    <a target="_blank" href="https://instagram.com/texas_hamburgueria" class="button display-inline-block wow zoomIn">
                        Insta do texas
                        <small class="display-block">@texas_hamburgueria</small>
                    </a>
                </div>
            </div>
            <ul class="instaphotos galera wow fadeInDown wrap">
                @for($i=0;$i<10;$i++)
                    <li><a target="_blank" href="{{ $instagrams[$i]['link'] }}"><img src="{{ $instagrams[$i]['images']['standard_resolution']['url'] }}" /></a></li>
                @endfor
            </ul>
        </section>
    </div>
@endsection
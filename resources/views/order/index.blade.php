@extends('layouts.master')
@section('content')

<div id="internal" class="">
    <section>
        <div class="wrap">
            <!-- Categoria -->
            <div class="text-center">
                <!-- LOGIN -->
                <h3 class="section-title wow fadeInUp padding-small-bottom">
                    <span>★<b>★</b>★</span>
                    Finalizar pedido
                </h3>
                @include('includes._errors')
                <h4 class="section-subtitle wow fadeInUp">Itens no carrinho. Confira antes de finalizar.</h4>
                <ul class="cart-list form-register padding-small-top">
                    @foreach(Cart::all() as $item)
                        <li class="cart-item" data-raw-id="{{ $item->rawId() }}" data-product-id="{{ $item->id }}">
                            <div class="product-thumb">
                                <img src="{{ asset($item->img) }}" alt="">
                            </div>
                            <strong>{{ $item->name }}</strong>
                            <div class="qtd">
                                <a class="bt-product-remove ti-minus"> </a>
                                <span>{{ $item->qty }}</span>
                                <a class="bt-product-add ti-plus"> </a>
                            </div>
                            <b class="total-item">R$ {{ number_format($item->price * $item->qty,2,',','') }}</b>
                        </li>
                    @endforeach

                    <li class="cart-item delivery-fee">
                        <strong>Taxa de entrega</strong>
                        <b class="total-item">R$ {{ number_format($packing_fee,2,',','') }}</b>
                    </li>

                    <li class="cart-item total">
                        <b class="total"><small>Total:</small> <span>R$ {{ number_format(Cart::total(),2,',','') }}</span></b>
                    </li>
                </ul>

                <h4 class="section-subtitle wow fadeInUp padding-small-top">Escolha a forma de pagamento</h4>

                {{ Form::open(['url' => route('order.store'), 'method' => 'POST']) }}

                <ul class="padding-default-top form-register row">
                    <li class="col-6 padding-supersmall-right padding-smartphone-zero-right padding-small-bottom">
                        <div class="padding-small-bottom">
                            <label for="">Como deseja pagar?</label>
                            {{ Form::select('payment_mode',$payment_modes) }}
                        </div>

                        <div>
                            <label for="">Precisa de troco? (opcional)</label>
                            {{ Form::text('troco',null,['placeholder' => 'Ex: Troco para 100 reais','required' => true]) }}
                        </div>
                    </li>
                    <li class="col-6 padding-supersmall-left padding-smartphone-zero-left padding-small-bottom">
                        <label for="">Alguma observação? (opcional)</label>
                        {{ Form::textarea('obs',null,['cols' => 30,'rows' => 10,'placeholder' => 'Ex: Sanduíche sem alface']) }}
                    </li>
                </ul>

                <h4 class="section-subtitle wow fadeInUp">Selecione o endereço de entrega <a class="display-inline-block" href="{{ route('user.profile') }}">(Editar endereços)</a></h4>

                <ul class="padding-supersmall-top padding-small-bottom form-register row">
                    @foreach($user_addresses as $user_address)
                        <li class="col-6 padding-supersmall">
                            <div class="shipping-data">
                                <input class="shipping-select" value="{{ $user_address->id }}" id="opt1" name="user_address_id" type="radio" {{ $user_address->default ? 'checked' : '' }}>
                                <label class="end" for="opt1">
                                    {{ $user_address->address }}, {{ $user_address->number }}<br />
                                    {{ $user_address->district }} - {{ $user_address->city }}, {{ $user_address->state }} <br />
                                    {{ $user_address->zipcode }}
                                </label>
                            </div>
                        </li>
                    @endforeach

                    {{--<li class="col-6 padding-supersmall">--}}
                        {{--<div class="shipping-data">--}}
                            {{--<input class="shipping-select" id="opt1" name="user_address_id" type="radio" checked>--}}
                            {{--<label class="end" for="opt1">--}}
                                {{--Rua Presidente João Pessoa, 123<br />--}}
                                {{--Centro - Campina Grande, PB <br />--}}
                                {{--58400-002--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</li>--}}

                    {{--<li class="col-6 padding-supersmall">--}}
                        {{--<div class="shipping-data">--}}
                            {{--<input class="shipping-select" id="opt2" name="user_address_id" type="radio">--}}
                            {{--<label for="opt2" class="end">--}}
                                {{--Rua Presidente João Pessoa, 123<br />--}}
                                {{--Centro - Campina Grande, PB <br />--}}
                                {{--58400-002--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="col-6 padding-supersmall">--}}
                        {{--<div class="shipping-data">--}}
                            {{--<input class="shipping-select" id="opt3" name="user_address_id" type="radio" checked>--}}
                            {{--<label for="opt3" class="end">--}}
                                {{--Rua Presidente João Pessoa, 123<br />--}}
                                {{--Centro - Campina Grande, PB <br />--}}
                                {{--58400-002--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</li>--}}

                    {{--<li class="col-6 padding-supersmall">--}}
                        {{--<div class="shipping-data">--}}
                            {{--<input class="shipping-select" id="opt4" name="user_address_id" type="radio">--}}
                            {{--<label for="opt4" class="end">--}}
                                {{--Rua Presidente João Pessoa, 123<br />--}}
                                {{--Centro - Campina Grande, PB <br />--}}
                                {{--58400-002--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                </ul>

                <button class="button-small bt-submit display-inline-block margin-small-top margin-smartphone-zero-top">
                    <span><i>★</i><b>★</b><i>★</i></span>
                    Pronto, enviar meu pedido!
                    <span><i>★</i><strong>★</strong><i>★</i></span>
                </button>

                {{ Form::close() }}
            </div>
        </div>
    </section>
</div>
@endsection
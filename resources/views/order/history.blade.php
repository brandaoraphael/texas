@extends('layouts.master')
@section('content')

<ul id="internal" class="">
    <section>
        <ul class="wrap">
            <!-- Categoria -->
            <ul class="text-center">
                <!-- LOGIN -->
                <h3 class="section-title wow fadeInUp padding-supersmall-bottom">
                    <span>★<b>★</b>★</span>
                    Meus pedidos
                </h3>
                <h4 class="section-subtitle wow fadeInUp margin-small-bottom">Histórico de pedidos</h4>

                @if(count($orders)==0)
                    <div class="msgerro">
                        <i class="fa fa-check-circle"></i>
                        Você não possui pedidos
                    </div>
                @endif

                @foreach($orders as $order)

                    <div class="order-title row text-left">
                        Pedido #{{ sprintf('%02d', $order->id) }}
                        <small class="{{ \Helper::get_order_status_class($order->status)['class'] }}">
                            {{ \Helper::get_order_status_class($order->status)['text'] }}
                        </small>
                        <span class="float-right">
							{{ $order->created_at->format('d/m/Y - H:i') }}

							<i class="padding-supersmall-left ti-arrow-down"></i>
						</span>
                    </div>

                    <ul class="order-item cart-list form-register">
                        @foreach($order->items as $item)

                            <li class="cart-item">
                                <div class="product-thumb">
                                    <img src="{{ asset($item->product->img) }}" alt="">
                                </div>
                                <strong>{{ $item->product->name }}</strong>
                                <div class="qtd">
                                    <span>x{{ $item->qtd }}</span>
                                </div>
                                <b class="total-item">R$ {{ number_format($item->qtd * $item->value,2,',','') }}</b>
                            </li>

                        @endforeach

                        <li class="cart-item">
                            <strong>Taxa de entrega</strong>
                            <b class="total-item">R$ {{ number_format($order->delivery_fee,2,',','') }}</b>
                        </li>
                        <li class="cart-item total">
                            <b class="total-item"><small>Total:</small> R$ {{ number_format($order->total,2,',','') }}</b>
                        </li>
                    </ul>

                @endforeach
            </ul>
        </ul>
    </section>
</ul>
@endsection
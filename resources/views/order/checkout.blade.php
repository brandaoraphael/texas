@extends('layouts.master')
@section('content')
<div id="internal">
    <section>
        <div class="wrap text-center">
                <h3 class="section-title wow fadeInUp padding-supersmall-bottom">
                    <span>★<b>★</b>★</span>
                    Pedido realizado!
                </h3>
                <div class="checkout-icon">
                    <i class="ti-check"></i>
                </div>
                <div class="section-subtitle wow fadeInUp">Seu pedido #{{ $order->id }} será entregue em:</div>
                <!-- <div class="section-subtitle wow fadeInUp margin-small-bottom">
                    Um e-mail será enviado para <strong>{{ $user->email }}</strong> com todas as informações deste pedido.
                </div> -->
                 <div class="section-subtitle wow fadeInUp margin-small-bottom checkout-box">
                    <div>{{ $user_address->address }}, {{ $user_address->number . ($user_address->complement ? ' (' . $user_address->complement . ')' : '') }}</div>

                    <div>{{ $user_address->district }} - {{ $user_address->city }}, {{ $user_address->state }}</div>

                    <div>CEP: {{ $user_address->zipcode }}</div>
                </div>
                <label class="text-center wow fadeInUp">
                    <a href="{{ route('order.history') }}">Clique aqui para acompanhar o pedido.</a>
                </label>
        </div>
    </section>
</div>
@endsection
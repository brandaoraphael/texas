@extends('admin.layout')
@section('content')
    <!-- tabs - submenu -->
    <!-- conteúdo -->
    <div class="limite content">
        <section class="tabela">
            <h2 class="clearfix">
                <i class="fa fa-bell-o styled"></i>
                {{ isset($title) ? $title : 'Últimos pedidos' }}
            </h2>

            @include('includes._errors')

            <table class="clicavel">
                <thead>
                <th>Endereço</th>
                <th>Valor</th>
                <th>Forma de pagamento</th>
                <th>Status</th>
                <th>Atualizar status</th>
                <th>Tempo de espera</th>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr data-href="{{ route('admin.order.show',$order->id) }}" title="Clique para ver os detalhes">
                        <td>
                            <b>{{ $order->address->address }}, {{ $order->address->number . ($order->address->complement ? '( ' . $order->address->complement . ')' : '') }} - {{ $order->address->district }}</b>
                        </td>
                        <td>R$ {{ number_format($order->total,2,'.','') }}</td>
                        <td>{{ Config::get('daikon.payment_modes')[$order->payment_mode] }}</td>
                        <td>
                            <small class="{{ \Helper::get_order_status_class($order->status)['class'] }}">
                                {{ \Helper::get_order_status_class($order->status)['text'] }}
                            </small>
                        </td>
                        <td>
                            <a href="#" data-href="{{ route('admin.order.update_order_status',[$order->id,2]) }}" class="botao save minimo confirm">
                                Marcar como entregue
                            </a>
                        </td>
                        <td>
                            <b>{{ $order->status==0 || $order->status == 1 ? Carbon::parse($order->created_at)->diffInMinutes(Carbon::now()) . ' minutos' : '' }}</b>
                        </td>
                    </tr>
                @endforeach

                <tr class="divider">
                    <td class="txt-center" colspan="6">
                        {{ count(request()->instance()->query('orders')) == 1 ? count(request()->instance()->query('orders')) . ' pedido' : count(request()->instance()->query('orders')) . ' pedidos' }} em aberto
                    </td>
                </tr>
                </tbody>
            </table>
        </section>
        <nav class="pager">
            {{ $orders->appends(['status' => request()->get('status'), 'interval' => request()->get('interval'), 'search' => request()->get('search') ])->links() }}
        </nav>
    </div>

    <div class="confirmation-modal modal" tabindex="-1" role="dialog">
        <div class="">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Confirmation required</h4>
                </div>
                <div class="modal-body">Are you sure you want to delete that comment?</div>
                <div class="modal-footer">
                    <button class="confirm btn " type="button" data-dismiss="modal" data-href="">Marcar como entregue</button>
                    <button class="cancel btn " type="button" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
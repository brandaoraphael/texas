@extends('admin.layout')
@section('content')
    <!-- cadastrar cliente - inicio -->
    <div class="limite content">
        <section class="form">
            <h2 class="clearfix">
                <i class="fa fa-group styled"></i>
                Editar cliente
                <a class="botao menor" href="{{ route('admin.order.create') }}"><i class="fa fa-plus-square-o"></i>Fazer um novo pedido</a>
            </h2>
            <div class="box meusdados">
                @include('includes._errors')
                {{ Form::open(['route' => 'admin.user.store','method' => 'POST']) }}
                <ul>
                    <li>
                        {{ Form::label('Nome') }}
                        {{ Form::text('name',$user->name,['placeholder' => 'Inserir nome']) }}
                    </li>
                    <li>
                        {{ Form::label('E-mail') }}
                        {{ Form::text('email',$user->email,['placeholder' => 'Inserir e-mail do cliente']) }}
                    </li>
                    <li>
                        {{ Form::label('Telefone') }}
                        {{ Form::text('phone',$user->phone,['placeholder' => 'Inserir telefone do cliente','class' => 'phone']) }}
                    </li>
                    <li>
                        {{ Form::label('Data de nascimento') }}
                        {{ Form::text('birthday',Carbon::parse($user->birthday)->format('d/m/Y'),['placeholder' => 'DD/MM/AAAA','class' => 'date']) }}
                    </li>
                </ul>
                <div class="enderecos">
                    <ul>
                        <li>
                            {{ Form::label('CEP') }}
                            {{ Form::text('zipcode',null,['placeholder' => 'Digite o CEP','class' => 'zipcode','id' => 'zipcode']) }}
                        </li>
                        <li>
                            {{ Form::label('Bairro') }}
                            {{ Form::text('district',null,['placeholder' => 'Informe o bairro', 'id' => 'district']) }}
                        </li>
                        <li>
                            {{ Form::label('Endereço') }}
                            {{ Form::text('address',null,['placeholder' => 'Ex: Rua do Conde','id' => 'address']) }}
                        </li>
                        <li class="numero">
                            {{ Form::label('Número') }}
                            {{ Form::text('number',null,['placeholder' => 'Ex: 1002']) }}
                        </li>
                        <li class="complemento">
                            {{ Form::label('Complemento') }}
                            {{ Form::text('complement',null,['placeholder' => 'EX: Apto 203']) }}
                        </li>
                        <li>
                            {{ Form::label('Cidade') }}
                            {{ Form::text('city',null,['placeholder' => 'Informe a cidade','id' => 'city']) }}
                        </li>
                        <li>
                            {{ Form::label('Estado') }}
                            {{ Form::select('state',$states,null,['placeholder' => 'Selecione o estado','id' => 'state']) }}
                        </li>
                        <li>
                            {{ Form::submit('+ Adicionar endereço',['class' => 'botao']) }}
                        </li>
                    </ul>

                    <h1 class="clearfix">Editar endereços</h1>
                    <ul>
                        <li>
                            {{ Form::label('CEP') }}
                            {{ Form::text('zipcode','54430-160',['placeholder' => 'Digite o CEP','class' => 'zipcode','id' => 'zipcode']) }}
                        </li>
                        <li>
                            {{ Form::label('Bairro') }}
                            {{ Form::text('district','Candeias',['placeholder' => 'Informe o bairro', 'id' => 'district']) }}
                        </li>
                        <li>
                            {{ Form::label('Endereço') }}
                            {{ Form::text('address','Rua Amambai',['placeholder' => 'Ex: Rua do Conde','id' => 'address']) }}
                        </li>
                        <li class="numero">
                            {{ Form::label('Número') }}
                            {{ Form::text('number','198',['placeholder' => 'Ex: 1002']) }}
                        </li>
                        <li class="complemento">
                            {{ Form::label('Complemento') }}
                            {{ Form::text('complement','Apto 203',['placeholder' => 'EX: Apto 203']) }}
                        </li>
                        <li>
                            {{ Form::label('Cidade') }}
                            {{ Form::text('city','Jaboatão dos Guararapes',['placeholder' => 'Informe a cidade','id' => 'city']) }}
                        </li>
                        <li>
                            {{ Form::label('Estado') }}
                            {{ Form::select('state',$states,null,['placeholder' => 'Selecione o estado','id' => 'state']) }}
                        </li>
                        <li></li>
                        <li>
                            <button class="f-right botao delete" type="button">Remover endereço</button>
                        </li>
                    </ul>
                </div>
                <ul>
                    <li>
                        {{ Form::submit('Salvar alterações',['class' => 'botao']) }}
                        <a href="{{ route('admin.admin.index') }}" class="botao cancel">
                            Cancelar
                        </a>
                    </li>
                </ul>
                {{ Form::close() }}
            </div>
        </section>
    </div>
@endsection
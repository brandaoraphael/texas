@extends('admin.layout')
@section('content')
<!-- conteúdo -->
<div class="limite content">
	<section class="tabela">
		<h2 class="clearfix">
			<i class="fa fa-users styled"></i>
			Clientes cadastrados

			{{ Form::open(['name' => 'interval_form','method' => 'GET', 'url' => request()->url() ]) }}

			<div class="filter">
				Ordenar por:
				{{ Form::select('interval',['' => 'Mais recentes','old' => 'Mais antigos','alphabetic' => 'Ordem alfabetica'],request()->has('interval') && request()->get('interval') != null ? request()->get('interval') : null) }}
			</div>

			{{ Form::close() }}

			<div class="pesquisa">
				{{ Form::open(['name' => 'search','method' => 'GET', request()->url()]) }}
				{{ Form::text('search',request()->get('search'),['placeholder' => 'Pesquisar']) }}
				{{ Form::close() }}
				<i class="fa fa-search"></i>
			</div>
			<a class="botao menor" href="{{ route('admin.user.export') }}"><i class="fa fa-envelope-o"></i>Download emails</a>
			<a class="botao menor" href="{{ route('admin.user.create') }}"><i class="fa fa-plus-square-o"></i>Adicionar cliente</a>
		</h2>
		<table class="clicavel">
			<thead>
				<th>ID</th>
				<th>Nome</th>
				<th>E-mail</th>
				<th>Telefone</th>
				<th>Pedidos</th>
				<th>Cliente desde</th>
			</thead>
			<tbody>
			@foreach($users as $user)
				<tr data-href='{{ route('admin.user.edit',$user->id) }}' title="Clique para ver os detalhes" title="Clique para ver os detalhes">
					<td>#{{ $user->id }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->phone }}</td>
					<td>{{ count($user->orders) }}</td>
					<td>{{ $user->created_at->format('d/m/Y - H:i') }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</section>

	<nav class="pager">
		{{ $users->appends(['status' => request()->get('status'), 'interval' => request()->get('interval'), 'search' => request()->get('search') ])->links() }}
	</nav>
</div>
@endsection
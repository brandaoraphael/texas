@extends('admin.layout')
@section('content')

	<!-- conteúdo -->
	<div class="limite content">
		<section class="form">
			<h2 class="clearfix">
				<i class="fa fa-pencil-square-o styled"></i>
				Adicionar usuário
			</h2>
			<div class="box meusdados">

				@include('includes._errors')

				{{ Form::open(['url' => route('admin.admin.store'),'method' => 'POST']) }}

				<ul>
					<li>
						{{ Form::label('Nome') }}
						{{ Form::text('name',null,['placeholder' => 'Inserir nome']) }}
					</li>

					<li>
						{{ Form::label('E-mail') }}
						{{ Form::text('email',null,['placeholder' => 'Inserir e-mail de acesso']) }}
					</li>

					<li>
						{{ Form::label('Usuário') }}
						{{ Form::text('username',null,['placeholder' => 'Inserir usuário para acesso']) }}
					</li>

					<li>
						{{ Form::label('Nível de acesso') }}
						{{ Form::select('role',['1' => 'Atentende',2 => 'Administrador'],null) }}
					</li>
					<li>
						{{ Form::label('Senha') }}
						{{ Form::password('password',['placeholder' => 'Inserir a senha']) }}
					</li>
					<li>
						{{ Form::label('Confirmar senha') }}
						{{ Form::password('password_confirmation',['placeholder' => 'Confirmar a senha']) }}
					</li>
					<li>
						{{ Form::submit('Adicionar usuário',['class' => 'botao']) }}
						<a href="{{ route('admin.admin.index') }}" class="botao cancel">
							Cancelar
						</a>
					</li>
				</ul>
				{{ Form::close() }}
			</div>
		</section>
	</div>
@endsection
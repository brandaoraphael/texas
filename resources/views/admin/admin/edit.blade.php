@extends('admin.layout')
@section('content')

<!-- conteúdo -->
<div class="limite content">
	<section class="form">
		<h2 class="clearfix">
			<i class="fa fa-pencil-square-o styled"></i>
			Editar usuário
		</h2>
		<div class="box meusdados">

			@include('includes._errors')



			<ul>

				<li>
					{{ Form::open(['url' => route('admin.admin.update',$admin->id),'method' => 'PUT']) }}
					{{ Form::label('Nome') }}
					{{ Form::text('name',$admin->name,['placeholder' => 'Inserir nome']) }}
				</li>

				<li>
					{{ Form::label('E-mail') }}
					{{ Form::text('email',$admin->email,['placeholder' => 'Inserir e-mail de acesso']) }}
				</li>

				<li>
					{{ Form::label('Usuário') }}
					{{ Form::text('username',$admin->username,['placeholder' => 'Inserir usuário para acesso']) }}
				</li>

				<li>
					{{ Form::label('Nível de acesso') }}
					{{ Form::select('role',[1 => 'Atentende',0 => 'Administrador'],$admin->role) }}
				</li>
				<li>
					{{ Form::label('Alterar senha (opcional)') }}
					{{ Form::password('password',['placeholder' => 'Inserir nova senha']) }}
				</li>
				<li>
					{{ Form::label('Confirmar senha') }}
					{{ Form::password('password_confirmation',['placeholder' => 'Confirmar nova senha']) }}
				</li>
				<li>
					{{ Form::submit('Salvar alterações',['class' => 'botao']) }}
					<a href="{{ route('admin.admin.index') }}" class="botao cancel">
						Cancelar
					</a>
					{{ Form::close() }}
				</li>


				<li class="txt-right">
					{{ Form::open(['url' => route('admin.admin.destroy',$admin->id), 'method' => 'DELETE']) }}
					{{ Form::button('Remover usuário',['class' => 'botao delete']) }}
					{{ Form::close() }}
				</li>

			</ul>

		</div>
	</section>
</div>

<div class="confirmation-modal modal" tabindex="-1" role="dialog">
	<div class="">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Confirmação</h4>
			</div>
			<div class="modal-body">Tem certeza que deseja excluir o atentende?</div>
			<div class="modal-footer">
				<button class="confirm btn " type="button" data-dismiss="modal" data-href="">Excluir atendente</button>
				<button class="cancel btn " type="button" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>
@endsection
@extends('admin.layout')
@section('content')

<!-- conteúdo -->
<div class="limite content">
	<section class="tabela">
		<h2 class="clearfix">
			<i class="fa fa-pencil-square-o styled"></i>
			Usuários no sistema
			<div class="pesquisa">
				{{ Form::open(['name' => 'search','method' => 'GET', request()->url()]) }}
				{{ Form::text('search',request()->get('search'),['placeholder' => 'Pesquisar']) }}
				{{ Form::close() }}
				<i class="fa fa-search"></i>
			</div>
			<a class="botao menor" href="{{ route('admin.admin.create') }}"><i class="fa fa-plus-square-o"></i>Adicionar usuário</a>
		</h2>
		@include('includes._errors')
		<table class="clicavel">
			<thead>
				<th>Nome</th>
				<th>E-mail</th>
				<th>Nível de acesso</th>
				<th>Cadastrado em</th>
			</thead>
			<tbody>
				@foreach($admins as $admin)
				<tr data-href="{{ route('admin.admin.edit',$admin->id) }}" title="Clique para ver os detalhes">
					<td>{{ $admin->name }}</td>
					<td><a href="">{{ $admin->email }}</a></td>
					<td>{{ $admin->role == 0 ? 'Administrador' : 'Atendente' }}</td>
					<td>{{ $admin->created_at->format('d/m/Y - H:i') }}</td>
				</tr>
				@endforeach

			</tbody>
		</table>
	</section>
	<nav class="pager">
		{{ $admins->appends([ 'search' => request()->get('search') ])->links() }}
	</nav>
</div>
@endsection
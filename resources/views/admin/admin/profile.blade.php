@extends('admin.layout')
@section('content')
<!-- conteúdo -->
<div class="limite content">
	<section class="form">
		<h2 class="clearfix">
			<i class="fa fa-cog styled"></i>
			Meus dados
		</h2>
		<div class="box meusdados">
			@include('includes._errors')
			{{ Form::open(['url' => route('admin.profile'),'method' => 'POST']) }}
			<ul>
				<li>
					{{ Form::label('Nome') }}
					{{ Form::text('name',$admin->name,['placeholder' => 'Nome']) }}
				</li>
				<li>
					{{ Form::label('Usuário') }}
					{{ Form::text('username',$admin->username,['placeholder' => 'Usuário']) }}
				</li>
				<li>
					{{ Form::label('E-mail') }}
					{{ Form::email('email',$admin->email,['placeholder' => 'E-mail']) }}
				</li>
				<li></li>
				<li>
					{{ Form::label('Alterar senha') }}
					{{ Form::password('password',['placeholder' => 'Alterar senha']) }}
				</li>

				<li>
					{{ Form::label('Confirmar senha') }}
					{{ Form::password('password_confirmation',['placeholder' => 'Confirmar senha']) }}
				</li>

				<li>
					{{ Form::hidden('role',$admin->role) }}
					{{ Form::submit('Salvar Alterações',['class' => 'botao']) }}
					<a href="{{ route('admin.index') }}" class="botao cancel">
						Cancelar
					</a>
				</li>
			</ul>
			{{ Form::close() }}
		</div>
	</section>
</div>
@endsection
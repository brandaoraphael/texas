@extends('admin.layout')
@section('content')
<!-- tabs - submenu -->

@include('admin.include.header_product')

<!-- conteúdo -->
<div class="limite content">
	<section class="form">
		<h2 class="clearfix">
			<i class="fa fa-truck styled"></i>
			Taxa de entrega
		</h2>
		<div class="box meusdados">

			@include('includes._errors')

			{{ Form::open(['url' => route('admin.delivery.store'),'method' => 'post']) }}
			<ul class="row">
				@foreach($configs as $config)
				<li class="col-6">
					{{ Form::label($config->name . ' R$') }}
					{{ Form::text("config[" . $config->key . "]",$config->value,['placeholder' => $config->placeholder,'class' => 'money']) }}
				</li>
				@endforeach
			</ul>
			<div class="clearfix p-left-20 p-top-20">
				{{ Form::submit('Salvar alterações',['class' => 'botao']) }}
			</div>
			{{ Form::close() }}
		</div>
	</section>
</div>
@endsection
<div class="subheader">
    <div class="limite">
        <ol>
            <li>
                <a class="{{ request()->route()->getName() == 'admin.product.index' ? 'ativo' : '' }}" href="{{ route('admin.product.index') }}">
                    <i class="fa fa-cutlery"></i>
                    Pratos
                </a>
            </li>
            <li>
                <a class="{{ request()->route()->getName() == 'admin.category.index' ? 'ativo' : '' }}" href="{{ route('admin.category.index') }}">
                    <i class="fa fa-th-list"></i>
                    Categorias
                </a>
            </li>
            <li>
                <a class="{{ request()->route()->getName() == 'admin.delivery.index' ? 'ativo' : '' }}" href="{{ route('admin.delivery.index') }}">
                    <i class="fa fa-truck"></i>
                    Taxas de entrega
                </a>
            </li>
        </ol>
    </div>
</div>

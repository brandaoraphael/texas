<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Administrativo - Daikon Cardápio Digital</title>
	<link rel="stylesheet" href="{{ asset('assets/admin/css/style.css') }}">
	@if(isset($stylesheets))
		@foreach($stylesheets as $stylesheet)
			<link rel="stylesheet" href="{{ $stylesheet }}">
		@endforeach
	@endif
</head>
<body>
	{{ Form::hidden('base_url',url('/'),['id' => 'base_url']) }}
	<!-- header geral -->
	<header>
		<div class="limite">
			<!-- logo -->
			<a href="" class="logo">
				<img src="{{ asset('assets/admin/img/logo.png') }}">
			</a>
			<!-- navegação -->
			<nav>
				<ul>
					<li>
						<b class="new_orders">{{ count(request()->instance()->query('orders')) }}</b>
						<a class="{{ request()->route()->getName() == 'admin.index' ? 'ativo' : '' }}" href="{{ route('admin.index') }}">
							<i class="fa fa-home"></i>
							Início
						</a>
					</li>
					<li>
						<a class="{{ request()->route()->getPrefix() == 'admin/order' ? 'ativo' : '' }}" href="{{ route('admin.order.index') }}">
							<i class="fa fa-list"></i>
							Pedidos
						</a>
					</li>
					<li>
						<a class="{{ request()->route()->getPrefix() == 'admin/user' ? 'ativo' : '' }}" href="{{ route('admin.user.index') }}">
							<i class="fa fa-users"></i>
							Clientes
						</a>
					</li>
					<li>
						<a class="{{ request()->route()->getPrefix() == 'admin/product' || request()->route()->getPrefix() == 'admin/category' || request()->route()->getPrefix() == 'admin/delivery' ? 'ativo' : '' }}" href="{{ route('admin.product.index') }}">
							<i class="fa fa-file-text-o"></i>
							Cardápio
						</a>
					</li>
					<li>
						<a class="{{ request()->route()->getPrefix() == 'admin/admin' ? 'ativo' : '' }}" href="{{ route('admin.admin.index') }}">
							<i class="fa fa-pencil-square-o"></i>
							Atendentes
						</a>
					</li>
				</ul>
			</nav>
			<!-- user box -->
			<span>
				<strong class="userbox">
					{{ Auth::guard('admin')->user()->name }}
					<i class="fa fa-angle-down"></i>
				</strong>

				<ul>
					<li>
						<a href="{{ route('admin.profile') }}">
							<i class="fa fa-cog"></i>
							Meus dados
						</a>
					</li>
					<li>
						<a href="{{ route('admin.logout') }}">
							<i class="fa fa-sign-out"></i>
							Sair
						</a>
					</li>
				</ul>
			</span>
		</div>
	</header>
	
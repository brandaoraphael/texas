	<footer>
		&copy; 2016 SuperFood - Todos os direitos reservados
	</footer>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src='{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}'></script>
	<script src='{{ asset('assets/plugins/play-sound/jquery.playSound.js') }}'></script>
	<script src="{{ asset('assets/admin/js/functions.js') }}"></script>
	@if(isset($scripts))
		@foreach($scripts as $script)
		<script src='{{ $script }}'></script>
		@endforeach
	@endif
</body>
</html>
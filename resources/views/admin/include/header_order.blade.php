<div class="subheader">
    <div class="limite">
        <ol>
            <li><a class="{{ !request()->has('status') ? 'ativo' : '' }}" href="{{ route('admin.order.index') }}">Todos</a></li>
            <li>
                <a class="{{ request()->has('status') && request()->get('status')==0 ? 'ativo' : '' }}" href="{{ route('admin.order.index',    ['status' => 0]) }}">
                    <i class="fa fa-exclamation-triangle"></i>
                    Em aberto
                </a>
            </li>
            <li>
                <a class="{{ request()->has('status') && request()->get('status')==1 ? 'ativo' : '' }}" href="{{ route('admin.order.index',    ['status' => 1]) }}">
                    <i class="fa fa-motorcycle"></i>
                    Em trânsito
                </a>
            </li>
            <li>
                <a class="{{ request()->has('status') && request()->get('status')==2 ? 'ativo' : '' }}" href="{{ route('admin.order.index',    ['status' => 2]) }}">
                    <i class="fa fa-check-circle"></i>
                    Entregues
                </a>
            </li>
            <li>
                <a class="{{ request()->has('status') && request()->get('status')==3 ? 'ativo' : '' }}" href="{{ route('admin.order.index',    ['status' => 3]) }}">
                    <i class="fa fa-times-circle"></i>
                    Cancelados
                </a>
            </li>
        </ol>
    </div>
</div>
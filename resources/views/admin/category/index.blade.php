@extends('admin.layout')
@section('content')
<!-- tabs - submenu -->
@include('admin.include.header_product')
<!-- conteúdo -->
<div class="limite content">
	<section class="tabela">
		<h2 class="clearfix">
			<i class="fa fa-th-list styled"></i>
			Categorias
			{{--<div class="filter">--}}
				{{--Ordenar por:--}}
				{{--<select name="" id="">--}}
					{{--<option value="">Mais recentes</option>--}}
					{{--<option value="">Mais antigos</option>--}}
					{{--<option value="">Mais produtos</option>--}}
				{{--</select>--}}
			{{--</div>--}}

			<div class="pesquisa">
				{{ Form::open(['name' => 'search','method' => 'GET', request()->url()]) }}
				{{ Form::text('search',request()->get('search'),['placeholder' => 'Pesquisar']) }}
				{{ Form::close() }}
				<i class="fa fa-search"></i>
			</div>
			<a class="botao menor" href="{{ route('admin.category.create') }}"><i class="fa fa-plus-square-o"></i>Adicionar categoria</a>
		</h2>
		@include('includes._errors')
		<table class="clicavel">
			<thead>
				<th>Nome</th>
				<th>Descricão</th>
				<th>Produtos</th>
				<th>Cadastrado em</th>
			</thead>
			<tbody>
			@foreach($categories as $category)
				<tr data-href='{{ route('admin.category.edit',$category->id) }}' title="Clique para ver os detalhes">
					<td>{{ $category->name }}</td>
					<td>{{ $category->description }}</td>
					<td>{{ count($category->products) }}</td>
					<td>{{ $category->created_at->format('d/m/Y - H:i') }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</section>
	<nav class="pager">
		{{ $categories->appends(['status' => request()->get('status'), 'interval' => request()->get('interval'), 'search' => request()->get('search') ])->links() }}
	</nav>
</div>
@endsection
@extends('admin.layout')
@section('content')
<!-- conteúdo -->
<div class="limite content">
	<section class="form detalhes">
		<h2 class="clearfix">
			<i class="fa fa-th-list styled"></i>
			Editar categoria
		</h2>
		<div class="box meusdados">
			@include('includes._errors')
			{{ Form::open(['url' => route('admin.category.update',$category->id),'method' => 'PUT']) }}
			<ul>
				<li>
					{{ Form::label('Nome') }}
					{{ Form::text('name',$category->name,['placeholder' => 'Inserir nome da categoria']) }}
				</li>
				<li>
					{{ Form::label('Descrição (opcional)') }}
					{{ Form::text('description',$category->description,['placeholder' => 'Inserir descrição da categoria']) }}
				</li>
				<li>
					{{ Form::submit('Salvar alterações',['class' => 'botao']) }}
					<a href="{{ route('admin.category.index') }}" class="botao cancel">
						Cancelar
					</a>
				</li>
			</ul>
			{{ Form::close() }}
			<div class="clearfix p-left-20 p-top-20">


				{{ Form::open(['url' => route('admin.category.destroy',$category->id),'method' => 'DELETE', 'class' => 'display-inline']) }}
				{{ Form::button('Remover categoria',['class' => 'f-right botao delete']) }}
				{{ Form::close() }}
			</div>
		</div>
	</section>
</div>
<div class="confirmation-modal modal" tabindex="-1" role="dialog">
	<div class="">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Confirmação</h4>
			</div>
			<div class="modal-body">Tem certeza que deseja excluir a categoria?</div>
			<div class="modal-footer">
				<button class="confirm btn " type="button" data-dismiss="modal" data-href="">Excluir categoria</button>
				<button class="cancel btn " type="button" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>
@endsection
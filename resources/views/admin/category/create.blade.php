@extends('admin.layout')
@section('content')

<!-- conteúdo -->
<div class="limite content">
	<section class="form detalhes">
		<h2 class="clearfix">
			<i class="fa fa-th-list styled"></i>
			Adicionar categoria
		</h2>
		<div class="box meusdados">
			@if(isset($errors))
				@unless($errors->isEmpty())
					<ol class="msgerro">
						@foreach($errors->getMessages() as $error)
							<li><i class="fa fa-times-circle"></i> {{ $error[0] }}</li>
						@endforeach
					</ol>
				@endunless
			@endif
			@if(session()->has('success'))
				<div class="msgsucesso">
					<i class="fa fa-check-circle"></i>
					{{ session()->get('success') }}
				</div>
			@endif
			@if(session()->has('error'))
				<div class="msgerro">
					<i class="fa fa-check-circle"></i>
					{{ session()->get('error') }}
				</div>
			@endif
		
			{{ Form::open(['url' => route('admin.category.store'), 'method' => 'POST']) }}
			<ul>
				<li>
					{{ Form::label('Nome') }}
					{{ Form::text('name',null,['placeholder' => 'Inserir nome da categoria']) }}
				</li>
				<li>
					{{ Form::label('Descrição (opcional)') }}
					{{ Form::text('description',null,['placeholder' => 'Inserir descrição da categoria']) }}
				</li>
			</ul>
			<div class="clearfix p-left-20 p-top-20">
				{{ Form::submit('Adicionar categoria',['class' => 'botao']) }}
				<a href="{{ route('admin.category.index') }}" class="botao cancel">
					Cancelar
				</a>
			</div>
			{{ Form::close() }}
		</div>
	</section>
</div>
@endsection
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Painel - Daikon Cardápio Digital</title>
	<link rel="stylesheet" href="{{ asset('assets/admin/css/style.css') }}">
	<script src="{{ asset('assets/js/jquery-2.1.1.min.js') }}"></script>
	<script src="{{ asset('assets/admin/js/functions.js') }}"></script>
</head>
<body>
	<section class="login form">
		<div class="box">
			{{ Form::open(['url' => route('admin.login'),'method' => 'POST']) }}
			<ul>
				<li class="logo">
					<i><img src="{{ asset('assets/admin/img/logo.png') }}" alt=""></i>
					<h1>
						Cardápio Digital
						<strong>Painel Administrativo</strong>
					</h1>

				</li>
				@include('includes._errors')
				<li>
					{{ Form::label('Usuário') }}
					{{ Form::text('username',null,['placeholder' => 'Inserir usuário']) }}
				</li>
				<li>
					{{ Form::label('Senha') }}
					{{ Form::password('password',null,['placeholder' => 'Inserir senha']) }}
				{{--<span>Esqueceu a senha? <a href="">Clique para recuperar</a></span>--}}
				</li>
			</ul>
			<div class="clearfix">
				{{ Form::submit('Acessar o sistema',['class' => 'botao']) }}
			</div>
			{{ Form::close() }}
		</div>
	</section>
</body>
</html>
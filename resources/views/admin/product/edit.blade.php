@extends('admin.layout')
@section('content')

<!-- conteúdo -->
<div class="limite content">
	<section class="form detalhes">
		<h2 class="clearfix">
			<i class="fa fa-cutlery styled"></i>
			Editar prato
		</h2>
		@include('includes._errors')
		<div class="box meusdados">
			{{ Form::open(['url' => route('admin.product.update',$product->id),'method' => 'PUT','files' => true]) }}
			<ul>
				<li>
					{{ Form::label('Nome') }}
					{{ Form::text('name',$product->name,['placeholder' => 'Inserir nome do produto']) }}
				</li>
				<li>
					{{ Form::label('Descrição (opcional)') }}
					{{ Form::text('description',$product->description,['placeholder' => 'Inserir descrição do produto']) }}
				</li>
				<li>
					{{ Form::label('Valor unitário') }}
					{{ Form::text('value',number_format($product->value,2,'.',''),['placeholder' => 'R$ 00.00','class' => 'money']) }}
				</li>
				<li>
					{{ Form::label('Selecione a categoria') }}
					{{ Form::select('category_id',$categories,$product->category_id) }}
				</li>
			</ul>
			<ul>
				<li>
					<div id="imgpreview">
						<img src="{{ asset($product->img) }}" alt="" />
					</div>
					<div class="carregar">
						<b class="botao">
							Carregar imagem
							<input type="file" name="image" id="image" />
						</b>
						<span>Selecione uma imagem</span>
					</div>
				</li>
			</ul>
			<ul>
				<li>
					<input type="submit" class="botao" value="Salvar alterações">
					<a href="{{ route('admin.product.index') }}" class="botao cancel">
						Cancelar
					</a>
				</li>
			</ul>
			{{ Form::close() }}
			{{ Form::open(['url' => route('admin.product.destroy',$product->id),'id' => 'delete_product_' . $product->id,'method' => 'DELETE']) }}
			<div class="clearfix p-left-20 p-top-20">
				{{ Form::button('Remover produto',['class' => 'f-right botao delete']) }}
			</div>
			{{ Form::close() }}
		</div>
	</section>
</div>
<div class="confirmation-modal modal" tabindex="-1" role="dialog">
	<div class="">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Confirmation required</h4>
			</div>
			<div class="modal-body">Are you sure you want to delete that comment?</div>
			<div class="modal-footer">
				<button class="confirm btn " type="button" data-dismiss="modal" data-href="">Excluir produto</button>
				<button class="cancel btn " type="button" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>
@endsection

@extends('admin.layout')
@section('content')
<!-- tabs - submenu -->
@include('admin.include.header_product')
<!-- conteúdo -->
<div class="limite content">
	<section class="tabela">
		<h2 class="clearfix">
			<i class="fa fa-cutlery styled"></i>
			Pratos no cardápio
			{{ Form::open(['name' => 'interval_form','method' => 'GET', 'url' => request()->url() ]) }}

			<div class="filter">
				Ordenar por:
				{{ Form::select('interval',['category' => 'Categoria','new' => 'Mais recentes','old' => 'Mais antigos','des_value' => 'Maior valor','asc_value' => 'Menor valor'],request()->has('interval') && request()->get('interval') != null ? request()->get('interval') : null) }}
			</div>

			{{ Form::close() }}

			<div class="pesquisa">
				{{ Form::open(['name' => 'search','method' => 'GET', request()->url()]) }}
				{{ Form::text('search',request()->get('search'),['placeholder' => 'Pesquisar']) }}
				{{ Form::close() }}
				<i class="fa fa-search"></i>
			</div>
			<a class="botao menor" href="{{ route('admin.product.create') }}"><i class="fa fa-plus-square-o"></i>Adicionar produto</a>
		</h2>
		@include('includes._errors')
		<table class="clicavel">
			<thead>
				<th>Nome</th>
				<th>Categoria</th>
				<th>Valor</th>
				<th>Cadastrado em</th>
			</thead>
			<tbody>
			@foreach($products as $product)
				<tr data-href='{{ route('admin.product.edit',$product->id) }}' title="Clique para ver os detalhes">
					<td>{{ sprintf('%02d', $product->id) }} - {{ $product->name }}</td>
					<td>{{ $product->category->name }}</td>
					<td>R$ {{ number_format($product->value,2,'.','') }}</td>
					<td>{{ $product->created_at->format('d/m/Y - H:i') }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</section>
	<nav class="pager">
		{{ $products->appends(['status' => request()->get('status'), 'interval' => request()->get('interval'), 'search' => request()->get('search') ])->links() }}
	</nav>
</div>
@endsection
@extends('admin.layout')
@section('content')

<!-- conteúdo -->
<div class="limite content">
	<section class="form detalhes">
		<h2 class="clearfix">
			<i class="fa fa-cutlery styled"></i>
			Adicionar prato
		</h2>
		<div class="box meusdados">
			@include('includes._errors')
			{{ Form::open(['url' => route('admin.product.store'),'method' => 'POST','files' => true]) }}
			<ul>
				<li>
					{{ Form::label('Nome') }}
					{{ Form::text('name',null,['placeholder' => 'Inserir nome do produto']) }}
				</li>
				<li>
					{{ Form::label('Descrição (opcional)') }}
					{{ Form::text('description',null,['placeholder' => 'Inserir descrição do produto']) }}
				</li>
				<li>
					{{ Form::label('Valor unitário') }}
					{{ Form::text('value',null,['class' => 'money','placeholder' => 'R$ 00.00']) }}
				</li>
				<li>
					{{ Form::label('Selecione a Categoria') }}
					{{ Form::select('category_id',$categories) }}
				</li>
			</ul>
			<ul>
				<li>
					<div id="imgpreview">
						<img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" alt="" />
					</div>
					<div class="carregar">
						<b class="botao">
							Carregar imagem
							{{ Form::file('image',['id' => 'image','accept' => 'image/*']) }}
						</b>
						<span>Selecione uma imagem</span>
					</div>
				</li>
			</ul>
			<div class="clearfix p-left-20 p-top-20">
				{{ Form::submit('Adicionar produto',['class' => 'botao']) }}
				<a href="{{ route('admin.product.index') }}" class="botao cancel">
					Cancelar
				</a>
			</div>
			{{ Form::close() }}
		</div>
	</section>
</div>
<script>
	$(function () {
		var $image = $('#image');
		var cropBoxData;
		var canvasData;

		$('#modal').on('shown.bs.modal', function () {
			$image.cropper({
				autoCropArea: 0.5,
				built: function () {
					$image.cropper('setCanvasData', canvasData);
					$image.cropper('setCropBoxData', cropBoxData);
				}
			});
		}).on('hidden.bs.modal', function () {
			cropBoxData = $image.cropper('getCropBoxData');
			canvasData = $image.cropper('getCanvasData');
			$image.cropper('destroy');
		});
	});
</script>
@endsection
@extends('admin.layout')
@section('content')
<div class="limite content">
   <section class="form detalhes">
      <h2 class="clearfix">
         <i class="fa fa-list styled"></i>
         Fazer um novo pedido
      </h2>
      <div class="box meusdados">
         <ul class="userdata">
               <li>
                  <label for="">Selecionar cliente</label>
                  <input type="text" class="autocomplete-customers" placeholder="Pesquisar cliente" value="Raphael Brandão">
               </li>
               <li>
                  <div class="address">	
                     <label for="">Selecionar endereço</label>
                     <select name="" id="">
                        <option value="" >Rua João Suassuna, 123 - Centro</option>
                     </select>
                  </div>
               </li>
               <li>
                  <div class="address">
                     <label for="">Contato do Cliente</label>
                     <span class="bolder">81989877202</span>
                  </div>
               </li>
               <li>
                  <label for="">Forma de pagamento</label>
                  <select name="" id="">
                     <option value="" >Dinheiro</option>
                  </select>
               </li>
               <li>
                  <label for="">Precisa de troco?</label>
                  <input type="text" placeholder="Ex: Troco para 100 reais">
               </li>
               <li>
                  <label for="">Observações</label>
                  <textarea name="" id="" cols="4" rows="3"></textarea>		
               </li>
               <li>
                  <input class="botao" type="submit" value="Concluir pedido">
                  <a href="http://localhost:8000/admin/order" class="botao cancel">
                  Voltar
                  </a>
               </li>
         </ul>
         <div class="addprodutos" id="#carrinho">
            <strong class="bolder">Produtos no pedido</strong>
            <ul>
               <li>
                  <label for="">Adicionar produto</label>
                  <input class="autocomplete-products" type="text" placeholder="Pesquisar produto">
               </li>
               <li>
                  <table class="table-default margin-0">
                     <label for="">Produtos no carrinho</label>
                     <tbody class="shopping-cart items">
                        <tr>
                           <td class="align-top">
                              <strong>Hot Filadélfia</strong>
                           </td>
                           <td class="text-center align-top">
                              <div class="item-quantity">
                                 <b class="bt-product-add">
                                    -
                                 </b>
                                 <span>10</span>
                                 <b class="bt-product-add">
                                    +
                                 </b>
                              </div>
                           </td>
                           <td class="text-right align-top">R$20.00</td>
                        </tr>
                        <tr>
                           <td class="align-top">
                              <strong>Refrigerante lata</strong>
                           </td>
                           <td class="text-center align-top">
                              <div class="item-quantity">
                                 <b class="bt-product-add">
                                   -
                                 </b>
                                 <span>1</span>
                                 <b class="bt-product-add">
                                    +
                                 </b>
                              </div>
                           </td>
                           <td class="text-right align-top">R$5.00</td>
                        </tr>
                     </tbody>
                  </table>
                  <table class="table-default margin-0">
                     <tbody class="shopping-cart">
                        <tr>
                           <td class="align-top">
                              Taxa de entrega
                           </td>
                           <td class="text-right align-top">R$8.00</td>
                        </tr>
                        <tr>
                           <td class="align-top">
                              Emabalagem
                           </td>
                           <td class="text-right align-top">R$0.00</td>
                        </tr>
                        <tr class="prod-total">
                           <td class="align-top">
                              Subtotal
                           </td>
                           <td class="text-right align-top">
                             <strong>R$33.00</strong>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </li>
            </ul>
         </div>
      </div>
   </section>
</div>
@endsection
@extends('admin.layout')
@section('content')
<!-- conteúdo -->
<div class="limite content">
	<section class="form detalhes">
		<h2 class="clearfix">
			<i class="fa fa-list styled"></i>
			Detalhes do pedido <strong>#{{ sprintf('%02d', $order->id) }}</strong>
		</h2>
		<div class="box meusdados">
		@if($order->status==0)
		<div class="message alert">
			Este pedido está aguardando uma atualização de status. Caso tenha saído para entrega ou tenha sido concluído, altere o status abaixo.
		</div>
		@endif
			@include('includes._errors')
			<ul class="userdata">
				<li>
					<label for="">Nome</label>
					<span class="bolder">{{ $order->user->name }}</span>
				</li>
				<li>
					<div class="address">	
					<label for="">Endereço</label>
					<span>
						<strong>{{ $order->address->address }}, {{ $order->address->number . ($order->address->complement ? '( ' . $order->address->complement . ')' : '') }} - {{ $order->address->district }}</strong>
					</span>
					</div>
				</li>
				<li>
					<div class="address">
						<label for="">Contato do Cliente</label>
						<span class="bolder">{{ $order->user->phone }}</span>
					</div>
				</li>
				<li>
					<label for="">Data/Hora</label>
					<span class="bolder">{{ $order->created_at->format('d/m/Y H:i:s') }}</span>
				</li>
				<li>
					<label for="">Forma de pagamento</label>
					<span class="bolder">{{ Config::get('daikon.payment_modes')[$order->payment_mode] }}
					</span>
				</li>

				@if($order->change)
				<li>
					<label for="">Troco</label>
					<span class="bolder">{{ $order->change }}</span>
				</li>
				@endif
				{{ Form::open(['url' => route('admin.order.update',$order->id), 'method' => 'PUT']) }}
				<li>
					<label for="">Status</label>
					<select name="status" id="status">
						<option value="0" {{ $order->status == 0 ? 'selected' : '' }}>Aguardando</option>
						<option value="1" {{ $order->status == 1 ? 'selected' : '' }}>Saiu para entrega</option>
						<option value="2" {{ $order->status == 2 ? 'selected' : '' }}>Entregue</option>
						<option value="3" {{ $order->status == 3 ? 'selected' : '' }}>Cancelado</option>
					</select>
				</li>
				<li>
					{{ Form::submit('Atualizar pedido',['class' => 'botao']) }}
					<a href="{{ url()->previous() != request()->url() ? url()->previous() : route('admin.index') }}" class="botao cancel">
						Voltar
					</a>
				</li>
				{{ Form::close() }}
			</ul>

			<div class="carrinho" id="#carrinho">
				<div>
					<button class="botao imprimir-tudo print-command"><i class="fa fa-print"></i> IMPRIMIR COMANDA</button>
					<button class="botao imprimir-tudo print-receipt"><i class="fa fa-print"></i> IMPRIMIR RECIBO</button>
				</div>
				<section id="command">
					@if($order->obs)
					<ul id="obs">
						<li><span class="f-left"><strong>Obs: </strong>{{ $order->obs }}</span></li>
					</ul>
					@endif
					@foreach($categories as $category)
					<ul>
						<li>
							<h3>{{ $category->name }}</h3>
							<button class="botao imprimir"><i class="fa fa-print"></i> IMPRIMIR</button>
						</li>
						@foreach($category->order_items($order->id,$category->id) as $item)
							<li>
								<label for="">
									<b>{{ $item->qtd }}</b> #{{ $item->product_id }} - {{ $item->name }}
								</label>
								<span class="values">R$ {{ number_format($item->qtd * $item->value,2,',','') }}</span>
							</li>
						@endforeach
					</ul>
					@endforeach
					<ul class="values">
						<li>
							<label for="">Subtotal</label>
							<span>R$ {{ number_format($order->value,2,',','') }}</span>
						</li>
						<li>
							<label for="">Taxa de entrega</label>
							<span>R$ {{ number_format($order->delivery_fee,2,',','') }}</span>
						</li>
                        <li>
                            <label for="">Taxa de embalagem</label>
                            <span>R$ {{ number_format($order->packing_fee,2,',','') }}</span>
                        </li>
						<li class="total">
							<label for="">Valor total</label>
							<span>R$ {{ number_format($order->total,2,',','') }}</span>
						</li>
					</ul>
				</section>
				<div class="">
					<button class="botao cancel imprimir-tudo print-view"><i class="fa fa-eye"></i> VISUALIZAR RECIBO</button>
				</div>
			</div>
		</div>
	</section>
</div>
<div class="confirmation-modal modal" tabindex="-1" role="dialog">
	<div class="">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Visualização do recibo</h4>
			</div>
			<div class="modal-body">
				<div class="dadosdorecibo">
					<div id="recibo">
						<pre>
							<p class="recibo center alto">{{ $dados_empresa['nome'] }}</p>
							<p class="recibo center">{{ $dados_empresa['razao'] }}</p>
							<p class="recibo center">{{ $dados_empresa['endereco'] }}, {{ $dados_empresa['numero'] . ($dados_empresa['complemento'] ? ' ' . $dados_empresa['complemento'] : '') }} - {{ $dados_empresa['bairro'] }}</p>
							<p class="recibo center">{{ $dados_empresa['cidade'] }} - {{ $dados_empresa['estado'] }} - CEP: {{ $dados_empresa['cep'] }}</p>
							<p class="recibo">{{ sprintf("%50s","__________________________________________________") }}</p>
							<p class="recibo">{{ $order->created_at->format('d/m/Y H:i:s') }}        NAO E DOCUMENTO FISCAL</p>
							<p class="recibo"></p>
							<p class="recibo obeso negrito center">RECIBO</p>
							<p class="recibo"></p>
							<p class="recibo">{{ sprintf("%3s",'COD') }} {{ sprintf("%-20s",'DESC') }} {{ sprintf("%3s",'QTD') }} {{ sprintf("%10s",'VL UNIT R$') }} {{ sprintf("%10s",'VL ITEM R$') }}</p>
							@foreach($order_items as $item)
								<p class="recibo">{{ sprintf("%3s",$item->product->id) }} {{ sprintf("%-20s",$item->product->name) }} {{ sprintf("%3s",$item->qtd) }} {{ sprintf("%10s",number_format($item->value,2,',','')) }} {{ sprintf("%10s",number_format($item->value * $item->qtd,2,',','') . 'c') }}</p>
							@endforeach
                            <p class="recibo">{{ sprintf("%50s","__________________________________________________") }}</p>
                            <p class="recibo">{{ sprintf("%-20s","Taxa de entrega") }} {{ sprintf("%28s",number_format($order->delivery_fee,2,',','')) }}c</p>
                            <p class="recibo">{{ sprintf("%-20s","Taxa de embalagem") }} {{ sprintf("%28s",number_format($order->packing_fee,2,',','')) }}c</p>
							<p class="recibo">{{ sprintf("%50s","__________________________________________________") }}</p>
							<p class="recibo gordo">{{ sprintf("%-5s","TOTAL") }}            {{ sprintf("%11s", "R$ " . number_format($order->total,2,',','')) }}</p>
							<p class="recibo"><p class="recibo">{{ sprintf("%-50s","Ta=" . number_format(\Helper::get_ta(),2,',','') . "%") }}</p>
							<p class="recibo"><p class="recibo">{{ sprintf("%-50s","Val.Aprox.Impostos R$ " . number_format(\Helper::calc_tributes($order->value),2,',','') . " (" . \Helper::get_ta_base() ."%)") }}</p>
							<p class="recibo"></p>
							<p class="recibo">NAO E DOCUMENTO FISCAL</p>
							<p class="recibo">{{ sprintf("%50s","__________________________________________________") }}</p>
							<p class="recibo">                                        LOJA:0001</p>
							<p class="recibo">{{ $dados_empresa['url'] }}      {{ $order->created_at->format('d/m/Y H:i:s') }}</p>
						</pre>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="confirm btn print-receipt" type="button" data-dismiss="modal" data-href="">Imprimir</button>
				<button class="cancel btn " type="button" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>
@endsection
@extends('admin.layout')
@section('content')
<!-- tabs - submenu -->
@include('admin.include.header_order')
<!-- conteúdo -->
<div class="limite content">
	<section class="tabela">
		<h2 class="clearfix">
			<i class="fa fa-list styled"></i>
			{{ isset($title) ? $title : 'Todos os pedidos' }}
			{{ Form::open(['name' => 'interval_form','method' => 'GET', 'url' => request()->url() ]) }}
			<div class="filter">
				Filtrar período:
				{{ Form::select('interval',['' => 'Últimos 7 dias',15 => 'Últimos 15 dias',30 => 'Últimos 30 dias',0 => 'Todos'],request()->has('interval') && request()->get('interval') != null ? request()->get('interval') : null) }}
			</div>
			{{ Form::close() }}
			<div class="pesquisa">
				{{ Form::open(['name' => 'search','method' => 'GET', request()->url()]) }}
				{{ Form::text('search',request()->get('search'),['placeholder' => 'Pesquisar']) }}
				{{ Form::close() }}
				<i class="fa fa-search"></i>
			</div>
			 <a class="botao menor" href="{{ route('admin.order.create') }}"><i class="fa fa-plus-square-o"></i>Fazer um novo pedido</a>
		</h2>
		<table class="clicavel">
			<thead>
				<th>ID</th>
				<th>Cliente</th>
				<th>Endereço</th>
				<th>Valor</th>
				<th>Status</th>
				<th>Data/Hora</th>
				<th>Tempo em espera</th>
			</thead>
			<tbody>
				@foreach($orders as $order)
				<tr data-href="{{ route('admin.order.show',$order->id) }}" title="Clique para ver os detalhes">
					<td><a href="">#{{ sprintf('%02d', $order->id) }}</a></td>
					<td>{{ $order->user->name }}</td>
					<td>{{ $order->user->address->address }}</td>
					<td>R$ {{ number_format($order->total,2,'.','') }}</td>
					<td>
						<small class="{{ \Helper::get_order_status_class($order->status)['class'] }}">
							{{ \Helper::get_order_status_class($order->status)['text'] }}
						</small>
					</td>
					<td>
						{{ $order->created_at->format('d/m/Y - H:i') }}
					</td>
					<td>
						{{ $order->status==0 || $order->status == 1 ? Carbon::parse($order->created_at)->diffInMinutes(Carbon::now()) . ' minutos' : '' }}
					</td>
				</tr>
				@endforeach

			</tbody>
		</table>
	</section>

	<nav class="pager">
		{{ $orders->appends(['status' => request()->get('status'), 'interval' => request()->get('interval'), 'search' => request()->get('search') ])->links() }}
	</nav>
</div>
@endsection
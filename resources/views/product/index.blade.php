@extends('layouts.master')
@section('content')
    <div id="shopping" class="">
        <section>
            <div class="wrap">
				@foreach($categories as $category)
					<!-- Categoria -->
					<div class="text-center">
						@include('includes._errors')
						<h3 class="section-title wow fadeInUp margin-default-bottom margin-default-top">
							<span>★<b>★</b>★</span>
							{{ $category->name }}
						</h3>
						@if(count($category->products) > 0)
							<ul class="products wow fadeInDown padding-small-bottom">
								@foreach($category->products as $product)
									<li class="product bt-addcart" data-raw-id="{{ session()->has('product_raw_id_' . $product->id) ? session()->get('product_raw_id_' . $product->id) : '' }}" data-product-id="{{ $product->id }}">
										<span class="product-thumb">
											<div class="img">
												<img src="{{ asset($product->img) }}" alt="">
											</div>
										</span>
												<span class="product-info">
											<div class="product-name">{{ $product->name }}</div>
											<div class="product-description">
												{{ $product->description }}
											</div>
										</span>
										<span class="float-right">
											<div class="product-price">
												R$ <strong>{{ number_format($product->value,2,',','') }}</strong>
											</div>
											<div class="info-addcart" data-product-id="{{ $product->id }}">
												<i class="ti-shopping-cart"></i> Adicionar ao carrinho
											</div>
										</span>
									</li>
								@endforeach
							</ul>
						@endif
					</div>
				@endforeach
            </div>
        </section>
    </div>
@endsection
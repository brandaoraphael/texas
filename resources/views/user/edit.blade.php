@extends('layouts.master')
@section('content')
    <div id="internal" class="">
        <section>
            <div class="wrap">
                <!-- Categoria -->
                <div class="text-center">
                    <!-- LOGIN -->
                    <h3 class="section-title wow fadeInUp padding-small-bottom">
                        <span>★<b>★</b>★</span>
                        Editar dados
                    </h3>

                    <h4 class="section-subtitle wow fadeInUp">DADOS PESSOAIS</h4>

                    {{ Form::open(['url' => route('user.update',$user->id),'method' => 'POST']) }}

                    <ul class="padding-default-top padding-small-bottom form-register row">
                        <li class="col-6 padding-supersmall-right padding-smartphone-zero-right padding-small-bottom">
                            {{ Form::label('Nome') }}
                            <div class="input">
                                <i class="ti-user"></i>
                                {{ Form::text('name',Auth::user()->name,['class' => 'display-block','placeholder' => 'Ex: João Dias']) }}
                            </div>
                        </li>
                        <li class="col-6 padding-supersmall-left padding-smartphone-zero-left padding-small-bottom">
                            {{ Form::label('E-mail (usuário)') }}
                            <div class="input">
                                <i class="ti-email"></i>
                                {{ Form::email('email',Auth::user()->email,['class' => 'display-block','placeholder' => 'email@provedor.com']) }}
                            </div>
                        </li>
                        <li class="col-6 padding-supersmall-right padding-smartphone-zero-right padding-small-bottom">
                            {{ Form::label('Telefone') }}
                            <div class="input">
                                <i class="ti-mobile"></i>
                                {{ Form::text('phone',Auth::user()->phone,['class' => 'display-block phone','placeholder' => 'Ex: (83) 3000-0000']) }}
                            </div>
                        </li>
                        <li class="col-6 padding-supersmall-left padding-smartphone-zero-left padding-small-bottom">
                            {{ Form::label('Data de nascimento') }}
                            <div class="input">
                                <i class="ti-gift"></i>
                                {{ Form::text('birthday',Carbon::parse(Auth::user()->birthday)->format('d/m/Y'),['class' => 'display-block date','placeholder' => 'DD/MM/AAAA']) }}
                            </div>
                        </li>
                        <li class="col-6 padding-supersmall-right padding-smartphone-zero-right padding-small-bottom">
                            {{ Form::label('Alterar senha') }}
                            <div class="input">
                                <i class="ti-unlock"></i>
                                {{ Form::password('password',['class' => 'display-block date','placeholder' => 'Insira sua nova senha']) }}
                            </div>
                        </li>
                        <li class="col-6 padding-supersmall-left padding-smartphone-zero-left padding-small-bottom">
                            {{ Form::label('Repetir nova senha') }}
                            <div class="input">
                                <i class="ti-unlock"></i>
                                {{ Form::password('password_confirmation',['class' => 'display-block date','placeholder' => 'Insira sua nova senha']) }}
                            </div>
                        </li>
                    </ul>

                    {{ Form::open(['name' => 'add_address','id' => 'add_address','url' => route('user.add_address')]) }}

                    <ul class="border-dashed padding-small form-register row">
                        <li>
                            <h4 class="section-subtitle wow fadeInUp padding-default-bottom">ADICIONAR ENDEREÇO</h4>
                        </li>
                        <li class="col-6 padding-supersmall-right padding-smartphone-zero-right padding-small-bottom">
                            {{ Form::label('CEP') }}
                            {{ Form::text('zipcode',null,['id' => 'zipcode', 'class' => 'display-block zipcode','placeholder' => 'Digite o CEP']) }}
                        </li>
                        <li class="col-6 padding-supersmall-left padding-smartphone-zero-left padding-small-bottom">
                            {{ Form::label('Bairro') }}
                            {{ Form::text('district',null,['id' => 'district', 'class' => 'display-block district','placeholder' => '']) }}
                        </li>
                        <li class="col-6 padding-supersmall-right padding-smartphone-zero-right padding-small-bottom">
                            {{ Form::label('Endereço') }}
                            {{ Form::text('address',null,['id' => 'address', 'class' => 'display-block address','placeholder' => '']) }}
                        </li>
                        <li class="col-6 padding-supersmall-left padding-smartphone-zero-left padding-small-bottom row">
                            <div class="col-5 padding-small-right padding-smartphone-zero-right padding-smartphone-small-bottom">
                                {{ Form::label('Número') }}
                                {{ Form::text('number',null,['id' => 'number', 'class' => 'display-block','placeholder' => 'Ex: 1002']) }}
                            </div>
                            <div class="col-7">
                                {{ Form::label('Complemento') }}
                                {{ Form::text('complement',null,['id' => 'complement', 'class' => 'display-block','placeholder' => 'Ex: Apto 203']) }}
                            </div>
                        </li>
                        <li class="col-6 padding-supersmall-right padding-smartphone-zero-right padding-small-bottom">
                            {{ Form::label('Cidade') }}
                            {{ Form::text('city',null,['id' => 'city', 'class' => 'display-block city','placeholder' => '']) }}
                        </li>
                        <li class="col-6 padding-supersmall-left padding-smartphone-zero-left padding-small-bottom">
                            {{ Form::label('Estado') }}
                            {{ Form::select('state',$states,null,['id' => 'state', 'class' => 'state', 'placeholder' => '-- Selecione --']) }}
                        </li>
                        <li class="col-12 padding-supersmall-top">
                            <button type="submit" class="button-small display-inline-block">
                                Adicionar novo endereço
                            </button>
                        </li>
                    </ul>

                    {{ Form::close() }}

                    <h4 class="section-subtitle wow fadeInUp padding-small-top">ENDEREÇOS CADASTRADOS</h4>

                    <ul class="padding-supersmall-top padding-small-bottom form-register row">
                        @foreach($addresses as $address)
                        <li class="col-6 padding-supersmall">
                            <div class="shipping-data">
                                <input class="shipping-select" id="opt1" name="user_address_id" type="radio" {{ $address->default ? 'checked' : '' }}>
                                <label for="opt1" class="end">
                                    <ul class="row">
                                        {{ Form::hidden("address_id[$address->id]",$address->id) }}
                                        <li class="col-6">
                                            {{ Form::text("update_zipcode[$address->id]",$address->zipcode,['class' => 'display-block zipcode', 'placeholder' => 'Digite o CEP']) }}
                                        </li>
                                        <li class="col-6">
                                            {{ Form::text("update_district[$address->id]",$address->district,['class' => 'display-block district', 'placeholder' => '']) }}
                                        </li>
                                        <li class="col-6">
                                            {{ Form::text("update_address[$address->id]",$address->address,['class' => 'display-block address', 'placeholder' => '']) }}
                                        </li>
                                        <li class="col-6 row">
                                            <div class="col-5 padding-supersmall-right padding-smartphone-supersmall-bottom padding-smartphone-zero-right">
                                                {{ Form::text("update_number[$address->id]",$address->number,['class' => 'display-block', 'placeholder' => 'Ex: 1002']) }}
                                            </div>
                                            <div class="col-7">
                                                {{ Form::text("update_complement[$address->id]",$address->zipcode,['class' => 'display-block', 'placeholder' => 'Ex: Apto 203']) }}
                                            </div>
                                        </li>
                                        <li class="col-6">
                                            {{ Form::text("update_city[$address->id]",$address->city,['class' => 'display-block city', 'placeholder' => '']) }}
                                        </li>
                                        <li class="col-6">
                                            {{ Form::select("update_state[$address->id]",$states,$address->state,['class' => 'state']) }}
                                        </li>
                                    </ul>
                                </label>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <button type="submit" class="button-small bt-submit display-inline-block margin-small-top">
                        <span><i>★</i><b>★</b><i>★</i></span>
                        Atualizar meus dados
                        <span><i>★</i><strong>★</strong><i>★</i></span>
                    </button>

                    {{ Form::close() }}
                </div>
            </div>
        </section>
    </div>
@endsection
@extends('layouts.master')
@section('content')

<div id="internal" class="">
    <section>
        <div class="wrap">
            <!-- Categoria -->
            <div class="text-center">
                <!-- LOGIN -->
                <h3 class="section-title wow fadeInUp padding-small-bottom">
                    <span>★<b>★</b>★</span>
                    Já é cadastrado?
                </h3>
                @include('includes._errors')
                {{ Form::open(['url' => route('user.login.verify'), 'method' => 'POST']) }}
                <ul class="form-login row">
                    @if(session()->has('login_error'))
                        <ol class="msgerro">
                            <li><i class="fa fa-times-circle"></i> {{ session()->get('login_error') }}</li>
                        </ol>
                    @endif
                    <li class="col-6 padding-supersmall-right">
                        <label for="">Usuário (E-mail)</label>
                        <div class="input">
                            <i class="ti-user"></i>
                            {{ Form::email('email',null,['class' => 'display-block', 'placeholder' => 'Informe o e-email']) }}
                            @if($errors->login->has('email'))
                            <span class="validator_error">{{ $errors->login->first('email') }}</span>
                            @endif
                        </div>
                    </li>
                    <li class="col-6 padding-supersmall-left">
                        <label for="">Senha</label>
                        <div class="input">
                            <i class="ti-unlock"></i>
                            {{ Form::password('password',null,['class' => 'display-block', 'placeholder' => 'Insira a senha']) }}
                            @if($errors->login->has('password'))
                                <span class="validator_error">{{ $errors->login->first('password') }}</span>
                            @endif
                        </div>
                    </li>
                    <li class="col-12 padding-small-top">
                        {{ Form::submit('Entrar com seus dados',['class' => 'button display-block']) }}
                    </li>
                    <label class="col-12 text-center padding-small-top">
                        Esqueceu a senha? <a href="">Recuperar aqui</a>
                    </label>
                </ul>
                {{ Form::close() }}

                <!-- CADASTRAR -->
                <h3 class="section-title wow fadeInUp margin-default-top">
                    <span>★<b>★</b>★</span>
                    Não tem uma conta?
                </h3>
                <h4 class="section-subtitle wow fadeInUp">Cadastre-se abaixo</h4>

                {{ Form::open(['url' => route('user.store'), 'method' => 'POST']) }}
                @if(session()->has('signup_error'))
                    <ol class="msgerro">
                        <li><i class="fa fa-times-circle"></i> {{ session()->get('signup_error') }}</li>
                    </ol>
                @endif
                <ul class="padding-default-top form-register row">
                    <li class="col-6 padding-supersmall-right padding-small-bottom">
                        {{ Form::label('Nome') }}
                        <div class="input">
                            <i class="ti-user"></i>
                            {{ Form::text('name',null,['class' => 'display-block', 'placeholder' => 'Ex. João Dias']) }}
                            @if($errors->signup->has('name'))
                                <span class="validator_error">{{ $errors->signup->first('name') }}</span>
                            @endif
                        </div>
                    </li>
                    <li class="col-6 padding-supersmall-left padding-small-bottom">
                        {{ Form::label('E-mail (usuário)') }}
                        <div class="input">
                            <i class="ti-email"></i>
                            {{ Form::email('email',null,['class' => 'display-block', 'placeholder' => 'Ex. email@provedor.com']) }}
                            @if($errors->signup->has('email'))
                                <span class="validator_error">{{ $errors->signup->first('email') }}</span>
                            @endif
                        </div>
                    </li>
                    <li class="col-6 padding-supersmall-right padding-small-bottom">
                        {{ Form::label('Telefone') }}
                        <div class="input">
                            <i class="ti-mobile"></i>
                            {{ Form::text('phone',null,['class' => 'display-block phone', 'placeholder' => 'Ex: (83) 3000-0000']) }}
                            @if($errors->signup->has('phone'))
                                <span class="validator_error">{{ $errors->signup->first('phone') }}</span>
                            @endif
                        </div>
                    </li>
                    <li class="col-6 padding-supersmall-left padding-small-bottom">
                        {{ Form::label('Data de nascimento') }}
                        <div class="input">
                            <i class="ti-gift"></i>
                            {{ Form::text('birthday',null,['class' => 'display-block date', 'placeholder' => 'DD/MM/AAAA']) }}
                            @if($errors->signup->has('birthday'))
                                <span class="validator_error">{{ $errors->signup->first('birthday') }}</span>
                            @endif
                        </div>
                    </li>
                    <li class="col-6 padding-supersmall-right padding-small-bottom">
                        {{ Form::label('Senha') }}
                        <div class="input">
                            <i class="ti-unlock"></i>
                            {{ Form::password('password',null,['class' => 'display-block', 'placeholder' => 'Insira a senha']) }}
                            @if($errors->signup->has('password'))
                                <span class="validator_error">{{ $errors->signup->first('password') }}</span>
                            @endif
                        </div>
                    </li>
                    <li class="col-6 padding-supersmall-left padding-small-bottom">
                        {{ Form::label('Repetir senha') }}
                        <div class="input">
                            <i class="ti-unlock"></i>
                            {{ Form::password('password_confirmation',null,['class' => 'display-block', 'placeholder' => 'Insira novamente a senha']) }}
                            @if($errors->signup->has('password_confirmation'))
                                <span class="validator_error">{{ $errors->signup->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </li>
                </ul>

                <h4 class="section-subtitle wow fadeInUp">ENDEREÇO DE ENTREGA</h4>
                <ul class="padding-default-top form-register row">
                    <li class="col-6 padding-supersmall-right padding-small-bottom">
                        {{ Form::label('CEP') }}
                        <div class="input">
                            <i class="ti-user"></i>
                            {{ Form::text('zipcode',null,['id' => 'zipcode', 'class' => 'display-block zipcode', 'placeholder' => 'Digite o CEP']) }}
                            @if($errors->signup->has('zipcode'))
                                <span class="validator_error">{{ $errors->signup->first('zipcode') }}</span>
                            @endif
                        </div>
                    </li>
                    <li class="col-6 padding-supersmall-left padding-small-bottom">
                        {{ Form::label('Bairro') }}
                        {{ Form::text('district',null,['id' => 'district', 'class' => 'display-block']) }}
                        @if($errors->signup->has('district'))
                            <span class="validator_error">{{ $errors->signup->first('district') }}</span>
                        @endif
                    </li>
                    <li class="col-6 padding-supersmall-right padding-small-bottom">
                        {{ Form::label('Endereço') }}
                        {{ Form::text('address',null,['id' => 'address', 'class' => 'display-block', 'placeholder' => 'Ex: Rua do Conde']) }}
                        @if($errors->signup->has('address'))
                            <span class="validator_error">{{ $errors->signup->first('address') }}</span>
                        @endif
                    </li>
                    <li class="col-6 padding-supersmall-left padding-small-bottom row">
                        <div class="col-5 padding-small-right">
                            {{ Form::label('Número') }}
                            {{ Form::text('number',null,['id' => 'number', 'class' => 'display-block', 'placeholder' => 'Ex: 1002']) }}
                            @if($errors->signup->has('number'))
                                <span class="validator_error">{{ $errors->signup->first('number') }}</span>
                            @endif
                        </div>
                        <div class="col-7">
                            {{ Form::label('Complemento') }}
                            {{ Form::text('complement',null,['id' => 'complement', 'class' => 'display-block', 'placeholder' => 'Ex: Apto 203']) }}
                            @if($errors->signup->has('complement'))
                                <span class="validator_error">{{ $errors->signup->first('complement') }}</span>
                            @endif
                        </div>
                    </li>
                    <li class="col-6 padding-supersmall-right padding-small-bottom">
                        {{ Form::label('Cidade') }}
                        {{ Form::text('city',null,['id' => 'city', 'class' => 'display-block', 'placeholder' => 'Informe a cidade']) }}
                        @if($errors->signup->has('city'))
                            <span class="validator_error">{{ $errors->signup->first('city') }}</span>
                        @endif
                    </li>
                    <li class="col-6 padding-supersmall-left padding-small-bottom">
                        {{ Form::label('Estado') }}
                        {{ Form::select('state',$states,null,['id' => 'state', 'placeholder' => '-- Selecione --']) }}
                        @if($errors->signup->has('state'))
                            <span class="validator_error">{{ $errors->signup->first('state') }}</span>
                        @endif
                    </li>
                    <li class="col-12 padding-small-top">
                        {{ Form::submit('Finalizar Cadastro',['class' => 'button display-block']) }}
                    </li>
                </ul>

                {{ Form::close() }}
            </div>
        </div>
    </section>
</div>
@endsection
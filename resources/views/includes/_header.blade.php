{{ Form::hidden('base_url',url()->to('/'),['id' => 'base_url']) }}
{{ Form::hidden('current_url',request()->segment(1),['id' => 'current_url']) }}
<header class="products">
	<div class="wrap row text-left">
		<ul class="col-6">
			<li>
				<a href="../" class="logo">
					<img src="{{ asset('assets/img/img-logo.png') }}" alt="">
				</a>
			</li>
			<li>
				<a class="voltar" href="{{ route('product.index') }}"><i class="pe-is-f-burger-2"></i> <span class="hide-smartphone">Ver</span> <span class="bt-vercardapio">cardápio</span></a>
			</li>
		</ul>
		<ul class="col-6 text-right float-right">
			<li>
				<!-- Botão carrinho -->
				<a class="nohover bt-cart-info" id="bt-cart-info-header" data-cart-count="{{ \Cart::count() }}"><i class="ti-shopping-cart"></i>
					(<span>{{ \Cart::count() }}</span>) <strong class="hide-smartphone">Itens no carrinho</strong>
				</a>
				<!-- Items no carrinho -->
				<div class="cart-items">
					<ul>
						@foreach(Cart::all() as $item)
						<li data-raw-id="{{ session()->has('product_raw_id_' . $item->id) ? session()->get('product_raw_id_' . $item->id) : '' }}" data-product-id="{{ $item->id }}">
							<b>
								<small class="bt-product-remove ti-minus"> </small>
								{{ $item->qty }}
								<small class="bt-product-add ti-plus"> </small>
							</b>
							<strong>
								{{ $item->name }}
								<span>R$ {{ number_format($item->total,2,',','') }}</span>
							</strong>
						</li>
						@endforeach
					</ul>
					<div class="taxa">
						Taxa de entrega
						<span>R$ {{ number_format(request()->instance()->query('delivery_fee'),2,',','') }}</span>
					</div>
					<div class="subtotal">
						Subtotal
						<span>R$ {{ number_format(request()->instance()->query('total'),2,',','') }}</span>
					</div>
					<a href="{{ route('order.index') }}" class="button display-block">Finalizar</a>
				</div>
			</li>
			<!-- USUÁRIO LOGADO POPOVER -->
			@if(Auth::check())
			<li>
				<!-- Botao usuario -->
				<a class="bt-user-info nohover">
					<i class="ti-user"></i> <span class="hide-smartphone">{{ Auth::user()->name }}</span> <i class="ti-angle-down"></i>
				</a>
				<!-- Menu do usuario logado -->
				<div class="user-items">
					<ul>
						<li>
							<a href="{{ route('product.index') }}"><i class="pe-is-f-burger-2"></i> Cardápio</a>
						</li>
						<li>
							<a href="{{ route('user.profile') }}"><i class="ti-settings"></i> Meus dados</a>
						</li>
						<li>
							<a href="{{ route('order.history') }}"><i class="ti-list"></i> Meus pedidos</a>
						</li>
						<li>
							<a href="{{ route('user.logout') }}"><i class="ti-power-off"></i> Sair</a>
						</li>
					</ul>
				</div>
			</li>
			@else
			<!-- USUÁRIO DESLOGADO -->
			<li>
				<a class="login" href="{{ route('user.login.form') }}">
					Entrar / Cadastrar
				</a>
			</li>
			@endif
		</ul>
	</div>
</header>
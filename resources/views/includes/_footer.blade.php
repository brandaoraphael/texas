<footer class="margin-default-top">
	<div class="wrap">
		<ul class="margin-default-right hide-smartphone">
			<li><img src="../assets/img/img-logo.png" alt=""></li>
		</ul>
		<ul class="margin-default-right display-smartphone-block text-smartphone-center">
			<li>
				<i class="ti-location-pin"></i>
				R. Janúncio Ferreira, 192
			</li>
			<li>
				<i class="ti-email"></i>
				<a href="">texasburgercg@gmail.com</a>
			</li>
			<li>
				<i class="ti-headphone-alt"></i>
				(83) 3341-6153
			</li>
			<li>
				<i class="ti-link"></i>
				<a href="http://texashamburgeria.com.br">texashamburgeria.com.br</a>
			</li>
		</ul>
		<ul class="display-smartphone-block text-smartphone-center">
			<li>Nossas redes</li>
			<li>
				<i class="ti-facebook"></i>
				<a href="https://facebook.com/TexasHA">facebook.com/TexasHA</a>
			</li>
			<li>
				<i class="ti-instagram"></i>
				<a href="https://instagram.com/texas_hamburgueria">@texas_hamburgueria</a>
			</li>
		</ul>
		<ul class="text-right float-right display-smartphone-block text-smartphone-center copy">
			<li>&copy; 2017 Texas Hamburgeria <br/>Todos os direitos reservados</li>
		</ul>
	</div>
</footer>
<script src="{{ asset('assets/js/jquery-2.1.1.min.js') }}"></script>
<script src="{{ asset('assets/js/wow.min.js') }}"></script>
<script src="{{ asset('assets/js/functions.js') }}"></script>
<script src="{{ asset('assets/js/page/product.js') }}"></script>
@if(isset($scripts))
	@foreach($scripts as $script)
		<script src="{{ $script }}"></script>
	@endforeach
@endif
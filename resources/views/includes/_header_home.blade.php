<header>
    <div class="wrap text-center">
        <div class="logomobile">
            <img src="{{ asset('assets/img/img-logo.png') }}" alt="">
        </div>
        <div class="navmobile">
            <span> </span>
        </div>
        <ul>
            <li class="subheader row ">
                <div class="col-6">
                    <span><i class="ti-headphone-alt"></i> (83) 3341-6153</span>
                    <span><i class="ti-email"></i> texasburgercg@gmail.com</span>
                </div>
                <div class="col-6 text-right">
					<span>
						<i class="ti-timer"></i>
						de Terça à Domingo das 17:00 às 00:00
					</span>
                </div>
            </li>
            <li data-wow-delay="0.9s"><a class="home" href="#inicio">Início</a></li>
            <li data-wow-delay="0.6s"><a href="#destaques">Destaques</a></li>
            <li data-wow-delay="0.3s"><a href='#sobre'>Sobre nós</a></li>
            <li class="logoparent hide-smartphone">
				<span class="logo">
					<img src="{{ asset('assets/img/img-logo.png') }}" alt="">
				</span>
            </li>
            <li data-wow-delay="0.3s"><a href="#delivery">Delivery</a></li>
            <li data-wow-delay="0.6s"><a href="#contato">Contato</a></li>
            <li data-wow-delay="0.9s"><a href="#insta">Instagram</a></li>
        </ul>
    </div>
</header>
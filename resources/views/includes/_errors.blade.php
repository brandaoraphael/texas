@if(isset($errors))
    @unless($errors->isEmpty())
        <ol class="msgerro">
            @foreach($errors->getMessages() as $error)
                <li><i class="fa fa-times-circle"></i> {{ $error[0] }}</li>
            @endforeach
        </ol>
    @endunless
@endif
@if(session()->has('success'))
    <ol class="msgsucesso">
        <li>
            <i class="fa fa-check-circle"></i>
            {{ session()->get('success') }}
        </li>
    </ol>
@endif
@if(session()->has('error'))
    <ol class="msgerro">
        <li><i class="fa fa-times-circle"></i> {{ session()->get('error') }}</li>
    </ol>
@endif
@if(session()->has('status'))
    <ol class="msgsucesso">
        <li>
            <i class="fa fa-check-circle"></i>
            {{ session()->get('status') }}
        </li>
    </ol>
@endif
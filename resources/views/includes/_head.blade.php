<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,maximum-scale=1,initial-scale=1,userscalabe=no">
<title>Texas Hamburgeria</title>
<link href="https://fonts.googleapis.com/css?family=Bangers|Patua+One|Rye" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

@if(isset($stylesheets))
	@foreach($stylesheets as $stylesheet)
		<link rel="stylesheet" href="{{ $stylesheet }}">
	@endforeach
@endif
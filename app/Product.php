<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    use SoftDeletes;

    protected $fillable = ['category_id','name','description','value','img'];

    protected $dates = [

        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function category(){

        return $this->belongsTo('App\Category');
    }

    public function order_items(){

        return $this->hasMany('App\OrderItem')->withTrashed();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    use SoftDeletes;

    protected $table = 'categories';

    protected $fillable = ['name','description'];

    protected $dates = [

        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function products(){

        return $this->hasMany('App\Product');
    }

    public function order_items($order_id,$category_id){

        return \DB::table('order_items')->select('order_items.qtd as qtd','products.id as product_id','products.name as name','order_items.value as value')
            ->join('products','order_items.product_id','=','products.id')
            ->join('categories','products.category_id','=','products.category_id')
            ->where('order_items.order_id',$order_id)
            ->where('products.category_id',$category_id)
            ->groupBy('order_items.id')
            ->get();
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{

    use SoftDeletes;

    protected $fillable = ['order_id','product_id','qtd','value'];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function product(){

        return $this->belongsTo('App\Product')->withTrashed();
    }

    public function products(){

        return $this->belongsToMany('App\Product','App\Category');
    }
}

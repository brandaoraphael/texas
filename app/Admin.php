<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Authenticatable
{

    use SoftDeletes;

    protected $fillable = ['name','email','username','password','remember_token','role'];

    protected $dates = ['created_at','updated_at','deleted_at'];

}

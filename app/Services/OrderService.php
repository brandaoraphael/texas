<?php
/**
 * Created by Marcus Vinicius de Carvalho.
 * E-mail:      marcuscarvalho6@gmail.com
 * Github:      marcuscarvalho6
 * Bibucket:    marcuscarvalho6
 * Date: 27/09/16
 * Time: 23:13
 */

namespace App\Services;


use App\OrderItem;
use App\Config;
use App\Order;
use App\Services\Abstracts\AbstractService;
use Auth;
use Cart;

class OrderService extends AbstractService
{

    public function order_store($data){

        try{

            \DB::beginTransaction();

            $user = Auth::user();

            if(!$user){

                return $this->error(null,'Cliente não encontrado',404);
            }

            $delivery_fee = floatval(Config::where('key','delivery_fee')->first()->value);



            $minimum_for_free_delivery = Config::where('key','minimum_for_free_delivery')->first()->value;

            if(Cart::total() >= floatval($minimum_for_free_delivery)){

                $delivery_fee = 0;
            }

            $cart_total = Cart::total();

            $packing_fee = 0;

            $config_packing_fee = Config::where('key','packing_fee')->first()->value;
            $config_packing_price = floatval(Config::where('key','packing_fee')->first()->value);

            if($config_packing_fee > 0 && $config_packing_price > 0)
                $packing_fee = ceil(floatval($cart_total) / $config_packing_fee) * $config_packing_price;

            $total = $cart_total + $delivery_fee + $packing_fee;

            $order_data = [

                'user_id'           => $user->id,
                'user_address_id'   => $data['user_address_id'],
                'payment_mode'      => $data['payment_mode'],
                'value'             => Cart::total(),
                'total'             => $total,
                'change'            => isset($data['troco']) && !empty($data['troco']) ? $data['troco'] : '',
                'obs'               => $data['obs'],
                'delivery_fee'      => $delivery_fee,
                'packing_fee'       => $packing_fee,
                'discount'          => 0,
                'status'            => 0
            ];

            $order = Order::create($order_data);

            if(!$order){

                \DB::rollback();
                return $this->error(null,'Erro ao tentar salvar o pedido');
            }

            $product_ids = [];


            foreach(Cart::all() as $item){

                $product_ids[] = $item->id;

                $order_item_data = [

                    'order_id'      => $order->id,
                    'product_id'    => $item->id,
                    'qtd'           => $item->qty,
                    'value'         => floatval($item->price)
                ];

                $order_item = OrderItem::create($order_item_data);

                if(!$order_item){

                    \DB::rollback();
                    return $this->error(null,'Erro ao tentar salvar os itens pedido');
                }

            }

            foreach($product_ids as $product_id){

                session()->forget('product_raw_id_' . $product_id);
            }

            \DB::commit();

            \Cart::destroy();

            return $this->success($order,'Pedido realizado com sucesso #' . $order->id);
        }

        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }

    }

    public function get_orders($data){


        $paginate = \Config::get('daikon.paginate');

        $order_query = Order::select('orders.*')
            ->join('users','users.id','=','orders.user_id')
            ->join('user_addresses','user_addresses.id','=','user_addresses.id');

        if(isset($data['status'])){

            if(is_array($data['status'])){

                $order_query->whereIn('orders.status',$data['status']);
            }

            else{

                $order_query->where('orders.status',$data['status']);
            }
        }

        if(isset($data['days'])) {

            $order_query->where('orders.created_at','>=',\Carbon::now()->subDays($data['days']));
        }

        if(isset($data['search']) && $data['search']){

            $order_query->where('users.name','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('orders.total','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('users.phone','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('users.email','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('user_addresses.address','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('user_addresses.number','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('user_addresses.complement','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('user_addresses.district','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('user_addresses.city','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('user_addresses.state','like','%'. urldecode($data['search']) .'%');
        }

        $order_query->groupBy('orders.id');

        return $order_query->paginate($paginate);
    }

    public function update_order($data){

        try{

            \DB::beginTransaction();

            $updated_order = Order::where('id',$data['id'])->update(['status' => $data['status']]);

            if(!$updated_order){

                \DB::rollback();
                return $this->error(null,'Erro ao tentar atualizar o pedido');
            }

            \DB::commit();

            return $this->success($updated_order,'Pedido atualizado com sucesso');
        }
        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }
}
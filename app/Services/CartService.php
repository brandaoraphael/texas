<?php
/**
 * Created by Marcus Vinicius de Carvalho.
 * E-mail:      marcuscarvalho6@gmail.com
 * Github:      marcuscarvalho6
 * Bibucket:    marcuscarvalho6
 * Date: 27/09/16
 * Time: 18:07
 */

namespace App\Services;


use App\Product;
use App\Services\Abstracts\AbstractService;
use Cart;
use App\Config;

class CartService extends AbstractService
{

    public function __construct(){

        Cart::associate('App\Product');
    }

    public function cart_add($data){

        try{

            $delivery_fee = Config::where('key','delivery_fee')->first()->value;
            $minimum_for_free_delivery = Config::where('key','minimum_for_free_delivery')->first()->value;

            $product = Product::find($data['product_id']);

            if(!$product){

                return $this->error(null,'Produto não localizado',404);
            }

            $row = Cart::add($data['product_id'], $product->name, 1, $product->value,['img' => $product->img]);

            session()->put('product_raw_id_' . $product->id,$row->rawId());

            $cart_total = Cart::total();

            if($cart_total >= floatval($minimum_for_free_delivery)){

                $delivery_fee = 0;
            }

            $packing_fee = 0;

            $config_packing_fee = Config::where('key','packing_fee')->first()->value;
            $config_packing_price = floatval(Config::where('key','packing_fee')->first()->value);

            if($config_packing_fee > 0 && $config_packing_price > 0)
                $packing_fee = ceil(floatval($cart_total) / $config_packing_fee) * $config_packing_price;

            $total = Cart::total() + $delivery_fee + $packing_fee;

            return $this->success(

                [
                    'raw_id'        => $row->rawId(),
                    'qty'           => $row->qty,
                    'price'         => $row->price,
                    'delivery_fee'  => number_format($delivery_fee,2,',',''),
                    'packing_fee'   => number_format($packing_fee,2,',',''),
                    'sub_total'     => number_format($row->price * $row->qty,2,',',''),
                    'total_items'   => Cart::count(),
                    'cart_all'      => Cart::all(),
                    'total'         => number_format($total,2,',','')
                ],

                'Produto adicionado com sucesso'
            );
        }

        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }

    public function cart_del($data){

        try{

            $delivery_fee = Config::where('key','delivery_fee')->first()->value;
            $minimum_for_free_delivery = Config::where('key','minimum_for_free_delivery')->first()->value;

            $product = Product::find($data['product_id']);

            if(!$product){

                return $this->error(null,'Produto não localizado',404);
            }

            $item = Cart::get($data['raw_id']);

            if($item->qty == 1){

                Cart::remove($item->rawId());
                session()->forget('product_raw_id_' . $product->id);

                $cart_total = Cart::total();

                if($cart_total >= floatval($minimum_for_free_delivery)){

                    $delivery_fee = 0;
                }

                $packing_fee = 0;

                $config_packing_fee = Config::where('key','packing_fee')->first()->value;
                $config_packing_price = floatval(Config::where('key','packing_fee')->first()->value);

                if($config_packing_fee > 0 && $config_packing_price > 0)
                    $packing_fee = ceil(floatval($cart_total) / $config_packing_fee) * $config_packing_price;

                $total = Cart::total() + $delivery_fee + $packing_fee;

                return $this->success(

                    [
                        'raw_id'        => null,
                        'qty'           => 0,
                        'price'         => 0,
                        'delivery_fee'  => number_format($delivery_fee,2,',',''),
                        'packing_fee'   => number_format($packing_fee,2,',',''),
                        'sub_total'     => number_format(0,2,',',''),
                        'total_items'   => Cart::count(),
                        'cart_all'      => Cart::all(),
                        'total'         => number_format($total,2,',','')
                    ],

                    'Produto removido com sucesso'
                );
            }

            $row = Cart::update($item->rawId(), isset($item) ? $item->qty-1 : 1);

            session()->put('product_raw_id_' . $product->id,$row->rawId());

            $cart_total = Cart::total();

            if($cart_total >= floatval($minimum_for_free_delivery)){

                $delivery_fee = 0;
            }

            $packing_fee = 0;

            $config_packing_fee = Config::where('key','packing_fee')->first()->value;
            $config_packing_price = floatval(Config::where('key','packing_fee')->first()->value);

            if($config_packing_fee > 0 && $config_packing_price > 0)
                $packing_fee = ceil(floatval($cart_total) / $config_packing_fee) * $config_packing_price;

            $total = $cart_total + $delivery_fee + $packing_fee;

            return $this->success(

                [
                    'raw_id'        => $row->rawId(),
                    'qty'           => $row->qty,
                    'price'         => $row->price,
                    'delivery_fee'  => number_format($delivery_fee,2,',',''),
                    'packing_fee'   => number_format($packing_fee,2,',',''),
                    'sub_total'     => number_format($row->price * $row->qty,2,',',''),
                    'total_items'   => Cart::count(),
                    'cart_all'      => Cart::all(),
                    'total'         => number_format($total,2,',','')
                ],

                'Produto removido com sucesso'
            );
        }

        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }

}
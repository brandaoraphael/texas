<?php
/**
 * Created by Marcus Vinicius de Carvalho.
 * E-mail:      marcuscarvalho6@gmail.com
 * Github:      marcuscarvalho6
 * Bibucket:    marcuscarvalho6
 * Date: 27/09/16
 * Time: 15:01
 */

namespace App\Services;


use App\Services\Abstracts\AbstractService;
use App\Admin;

class AdminService extends AbstractService
{

    public function admin_update($data,$id){

        try{

            \DB::beginTransaction();

            $admin_data = [

                'name'      => mb_strtoupper($data['name']),
                'username'  => mb_strtolower(\Helper::remove_accents($data['username'])),
                'email'     => mb_strtolower($data['email']),
                'password'  => \Hash::make($data['password']),
                'role'      => $data['role']
            ];


            $admin = Admin::updateOrCreate(['id' => $id],$admin_data);

            if(!$admin){

                return $this->error(null,'Ocorreu um erro ao tentar atualizar os dados do usuário');
            }

            \DB::commit();

            return $this->success($admin,'Usuário atualizado com sucesso');
        }
        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }

    }

    public function admin_store($data){

        \DB::beginTransaction();

        $admin_data = [

            'name'      => mb_strtoupper($data['name']),
            'username'  => mb_strtolower(\Helper::remove_accents($data['username'])),
            'email'     => mb_strtolower($data['email']),
            'password'  => \Hash::make($data['password']),
            'role'      => $data['role']
        ];


        $admin = Admin::create($admin_data);

        if(!$admin){

            return $this->error(null,'Ocorreu um erro ao tentar cadastrar o usuário');
        }

        \DB::commit();

        return $this->success($admin,'Usuário cadastrado com sucesso');
    }

    public function get_admins($data){

        $paginate = \Config::get('daikon.paginate');

        $order_query = Admin::select('admins.*');

        if(isset($data['search']) && $data['search']){

            $order_query->where('admins.id','like','%'. urldecode($data['search']) .'%');
            $order_query->orwhere('admins.name','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('admins.username','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('admins.email','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('admins.role','like','%'. urldecode($data['search']) .'%');
        }

        $order_query->groupBy('admins.id');

        return $order_query->paginate($paginate);
    }

    public function admin_destroy($id){

        try{

            \DB::beginTransaction();

            $admin = Admin::find($id);

            if(!$admin){

                return $this->error(null,'Atendente não encontrado');
            }

            if(!$admin->delete()){

                return $this->error(null,'Erro ao remover o atendente');
            }

            \DB::commit();

            return $this->success(null,'Atendente removido com sucesso');
        }
        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }
}
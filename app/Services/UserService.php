<?php
/**
 * Created by PhpStorm.
 * User: vinicius
 * Date: 14/09/16
 * Time: 02:30
 */

namespace App\Services;

use App\Services\Abstracts\AbstractService;
use App\User;
use App\UserAddress;
use Carbon\Carbon;

class UserService extends AbstractService
{

    public function store($data){

        try{

            $data['no_hash_password'] = $data['password'];

            $data['birthday']   = Carbon::createFromFormat('d/m/Y', $data['birthday'])->format('Y-m-d');
            $data['phone']      = \Helper::numbers_only($data['phone']);
            $data['zipcode']    = \Helper::numbers_only($data['zipcode']);
            $data['password']   = \Hash::make($data['password']);

            \DB::beginTransaction();

            $user = User::create($data);

            if(!$user){

                return $this->error(null,'Erro ao salvar as informações do usuário');
            }

            $data['user_id'] = $user->id;

            UserAddress::where('user_id',$user->id)
                ->where('default',1)
                ->update(['default' => 0]);

            $data['default'] = true;

            $user_address = UserAddress::create($data);

            if(!$user_address){

                return $this->error(null,'Erro ao salvar as informações do endereço');
            }

            if (!\Auth::attempt(['email' => $data['email'], 'password' => $data['no_hash_password']])) {

                return $this->error($user,'Login ou senha cadastrados são inválidos');
            }

            \DB::commit();

            return $this->success($user,'Usuário cadastrado com sucesso');
        }
        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }

    }

    public function store_address($data){

        try{

            $data['zipcode']    = \Helper::numbers_only($data['zipcode']);

            \DB::beginTransaction();

            UserAddress::where('user_id',$data['user_id'])
                ->where('default',1)
                ->update(['default' => 0]);

            $data['default'] = true;

            $user_address_data = [

                'user_id'       => $data['user_id'],
                'address'       => $data['address'],
                'number'        => $data['number'],
                'complement'    => $data['complement'],
                'district'      => $data['district'],
                'zipcode'       => $data['zipcode'],
                'city'          => $data['city'],
                'state'         => $data['state'],
                'default'       => true
            ];

            $user_address = UserAddress::updateOrCreate($user_address_data);

            if(!$user_address){

                return $this->error(null,'Erro ao salvar as informações do endereço');
            }

            \DB::commit();

            return $this->success($user_address,'Endereço cadastrado com sucesso');
        }
        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }

    }

    public function update($data){

        try{

            \DB::beginTransaction();

            $user = User::where('id',$data['id'])->first();

            if(!$user){

                return $this->error(null,'Usuário não encontrado');
            }

            $data['birthday'] = \Carbon::createFromFormat('d/m/Y',$data['birthday'])->format('Y-m-d');

            if($data['password']){

                $data['password'] = \Hash::make($data['password']);
            }

            $user_update = $user->update($data);

            if(!$user_update){

                \DB::rollback();
                return $this->error(null,'Erro ao salvar as informações do usuário');
            }

            foreach($data['address_id'] as $key=>$value) {


                $user_address_data = [

                    'user_id'       => $user->id,
                    'address'       => $data['update_address'][$key],
                    'number'        => $data['update_number'][$key],
                    'complement'    => $data['update_complement'][$key],
                    'district'      => $data['update_district'][$key],
                    'zipcode'       => $data['update_zipcode'][$key],
                    'city'          => $data['update_city'][$key],
                    'state'         => $data['update_state'][$key],
                    'default'       => $data['update_default']==$key ? 1 : 0
                ];

                $user_address = UserAddress::find($key);

                if(!$user_address->update($user_address_data)){

                    \DB::rollback();
                    return $this->success(null,'Erro ao atualizar os dados do endereço');
                }
            }

            \DB::commit();

            return $this->success(null,'Usuário atualizado com sucesso');
        }
        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }

    public function get_users($data){

        $paginate = \Config::get('daikon.paginate');

        $order_query = User::select('users.*');

        if(isset($data['interval']) && $data['interval']){

            if($data['interval'] == 'old'){

                $order_query->orderBy('users.created_at','desc');
            }

            if($data['interval'] == 'new'){

                $order_query->orderBy('users.created_at','asc');
            }

            if($data['interval'] == 'alphabetic'){

                $order_query->orderBy('users.name','asc');
            }

            if($data['interval'] == 'alphabetic'){

                $order_query->orderBy('users.name','asc');
            }
        }

        if(isset($data['search']) && $data['search']){

            $order_query->where('users.name','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('users.phone','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('users.email','like','%'. urldecode($data['search']) .'%');
        }

        $order_query->groupBy('users.id');

        return $order_query->paginate($paginate);
    }

    public function export()
    {
        $users = User::all();
        $columns = array('EMAIL');
        $callback = function() use ($users, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($users as $user) {
                fputcsv($file, array($user->email));
            }
            fclose($file);
        };
        return $callback;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Marcus
 * Date: 19/09/16
 * Time: 00:03
 */

namespace App\Services;

use App\Product;
use App\Services\Abstracts\AbstractService;
use Image;


class ProductService extends AbstractService
{

    public function product_store($data){

        try{

            \DB::beginTransaction();

            $image = $data['image'];
            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('products/' . $filename);

            Image::make($image->getRealPath())->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path);

            $product_data = [

                'category_id'   => $data['category_id'],
                'name'          => $data['name'],
                'description'   => $data['description'],
                'value'         => floatval($data['value']),
                'img'           => 'products/' . $filename
            ];

            $product = Product::updateOrCreate($product_data);

            if(!$product){

                return $this->error(null,'Erro ao salvar a categoria');
            }

            \DB::commit();

            return $this->success($product,'Produto cadastrado com sucesso');
        }
        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }

    public function product_update($data,$id){

        try{

            \DB::beginTransaction();

            $product_data = [

                'category_id'   => $data['category_id'],
                'name'          => $data['name'],
                'description'   => $data['description'],
                'value'         => floatval($data['value']),
            ];

            if(isset($data['image']) && !empty($data['image'])){

                $old_product_data = Product::find($id);

                if(!$old_product_data){

                    return $this->error(null,'Erro ao atualizar o produto');
                }

                unlink($old_product_data->img);

                $image = $data['image'];
                $filename  = time() . '.' . $image->getClientOriginalExtension();

                $path = public_path('products/' . $filename);

                Image::make($image->getRealPath())->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path);

                $product_data['img'] = 'products/' . $filename;
            }

            $product = Product::find($id)
                ->update($product_data);

            if(!$product){

                return $this->error(null,'Erro ao atualizar o produto');
            }

            \DB::commit();

            return $this->success($product,'Produto atualizado com sucesso');
        }

        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }

    public function product_destroy($id){

        try{

            \DB::beginTransaction();

            $product = Product::find($id);

            if(!$product){

                return $this->error(null,'Produto não encontrado');
            }

            if(!$product->delete()){

                return $this->error(null,'Erro ao remover o produto');
            }

            \DB::commit();

            return $this->success(null,'Produto removido com sucesso');
        }
        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }

    public function get_products($data){

        $paginate = \Config::get('daikon.paginate');

        $order_query = Product::select('products.*');
        $order_query->join('categories','products.category_id','=','categories.id');

        if(isset($data['interval']) && $data['interval']){

            if($data['interval'] == 'category'){

                $order_query->orderBy('products.category_id','asc');
            }

            if($data['interval'] == 'new'){

                $order_query->orderBy('products.created_at','desc');
            }

            if($data['interval'] == 'old'){

                $order_query->orderBy('products.created_at','asc');
            }

            if($data['interval'] == 'asc_value'){

                $order_query->orderBy('products.value','asc');
            }

            if($data['interval'] == 'des_value'){

                $order_query->orderBy('products.value','desc');
            }
        }

        if(isset($data['search']) && $data['search']){

            $order_query->where('products.id','like','%'. urldecode($data['search']) .'%');
            $order_query->orwhere('products.name','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('products.category_id','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('categories.name','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('categories.id','like','%'. urldecode($data['search']) .'%');
        }

        $order_query->groupBy('products.id');

        return $order_query->paginate($paginate);
    }
}
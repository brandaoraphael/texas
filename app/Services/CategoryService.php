<?php
/**
 * Created by PhpStorm.
 * User: Marcus
 * Date: 19/09/16
 * Time: 00:24
 */

namespace App\Services;

use App\Category;
use App\Services\Abstracts\AbstractService;

class CategoryService extends AbstractService
{

    public function store($data){

        try{

            \DB::beginTransaction();

            $category_data = [

                'name'          => $data['name'],
                'description'   => $data['description']
            ];

            $category = Category::updateOrCreate(['name' => $data['name']], $category_data);

            if(!$category){

                return $this->error(null,'Erro ao salvar a categoria');
            }

            \DB::commit();

            return $this->success($category,'Categoria cadastrada com sucesso');
        }
        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }

    public function category_update($data,$id){

        try{

            \DB::beginTransaction();

            $category_data = [

                'name'          => $data['name'],
                'description'   => $data['description']
            ];

            $category = Category::find($id);

            if(!$category){

                return $this->error(null,'Categoria não localizada');
            }

            if(!$category->update($category_data)){

                return $this->error(null,'Erro ao atualizar a categoria');
            }

            \DB::commit();

            return $this->success($category,'Categoria atualizada com sucesso');

        }
        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }

    public function get_categories($data){


        $paginate = \Config::get('daikon.paginate');

        $order_query = Category::select('*');

        if(isset($data['search']) && $data['search']){

            $order_query->where('categories.id','like','%'. urldecode($data['search']) .'%');
            $order_query->orwhere('categories.name','like','%'. urldecode($data['search']) .'%');
            $order_query->orWhere('categories.description','like','%'. urldecode($data['search']) .'%');
        }

        return $order_query->paginate($paginate);
    }

    public function category_destroy($id){

        try {

            \DB::beginTransaction();

            $category = Category::find($id);

            if (!$category) {

                return redirect()->back()->with('error', 'Categoria não localizada');
            }

            if (count($category->products) > 0) {

                return redirect()->back()->with('error', 'Essa categoria não pode ser excluída porque existem produtos associados a ela');
            }

            if (!$category->delete()) {

                return redirect()->back()->with('error', 'Erro ao excluir a categoria');
            }

            \DB::commit();

            return $this->success(null, 'Categoria excluída com sucesso');

        }
        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }

}
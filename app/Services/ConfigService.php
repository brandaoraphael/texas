<?php
/**
 * Created by Marcus Vinicius de Carvalho.
 * E-mail:      marcuscarvalho6@gmail.com
 * Github:      marcuscarvalho6
 * Bibucket:    marcuscarvalho6
 * Date: 27/09/16
 * Time: 12:35
 */

namespace App\Services;


use App\Config;
use App\Services\Abstracts\AbstractService;

class ConfigService extends AbstractService
{

    public function __construct()
    {
    }

    public function update_config($data){

        try{

            \DB::beginTransaction();

            foreach($data['config'] as $key=>$value){

                $config = Config::updateOrCreate(['key' => $key],

                    [
                        'value' => $value
                    ]
                );

                if(!$config){

                    return $this->error(null,'Ocorreu um erro ao salvar a configuração');
                }
            }

            \DB::commit();

            return $this->success(null,'Configurações atualizadas com sucesso');
        }

        catch(Exception $e){

            return $this->error(null,$e->getMessage(),$e->getCode());
        }
    }
}
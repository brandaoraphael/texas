<?php

namespace App\Services\Abstracts;

abstract class AbstractService{

    private $error = false;
    private $msg = "Solicitação concluida com sucesso";
    private $dados = array();

    public function error($dados, $msg = null){

        $this->error = true;
        $this->msg = "Falha na solicitação";

        if($msg)
            $this->msg = $msg;

        return array('error' => $this->error, 'dados' => $dados, 'msg' => $this->msg);

    }

    public function success($dados, $msg = null){

        if($msg)
            $this->msg = $msg;

        return array('error' => $this->error, 'dados' => $dados, 'msg' => $this->msg);

    }

}
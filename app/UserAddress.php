<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends Model
{
    use SoftDeletes;

    protected $table = 'user_addresses';

    protected $dates = [

        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $fillable = ['user_id','address','number','complement','district','city','state','zipcode','default'];
}

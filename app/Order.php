<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{

    use SoftDeletes;

    protected $fillable = ['user_id','user_address_id','payment_mode','value','total','change','obs','delivery_fee','packing_fee','discount','status'];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function user()
    {

        return $this->belongsTo('App\User');
    }

    public function items()
    {

        return $this->hasMany('App\OrderItem');
    }

    public function products()
    {

        return $this->belongsToMany('App\Product','order_items');
    }

    public function address()
    {

        return $this->belongsTo('App\UserAddress', 'user_address_id', 'id');
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAddressStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'zipcode'                   => 'required|min:10|max:10',
            'district'                  => 'required|max:50',
            'address'                   => 'required|max:100',
            'number'                    => 'required|max:20',
            'complement'                => 'max:50',
            'city'                      => 'required|max:100',
            'state'                     => 'required'
        ];
    }
}

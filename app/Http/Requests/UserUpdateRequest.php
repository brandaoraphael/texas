<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [

            'name'                             => 'required|min:5|max:100',
            'email'                            => 'required|email|unique:users,email,' . $request->segment(2),
            'phone'                            => 'required|min:14|max:15',
            'password'                         => 'min:6|max:50|confirmed',
            'password_confirmation'            => 'min:6|max:50',
            'update_zipcode'                   => 'required|max:10',
            'update_district'                  => 'required|max:50',
            'update_address'                   => 'required|max:100',
            'update_number'                    => 'required|max:20',
            'update_complement'                => 'max:50',
            'update_city'                      => 'required|max:100',
            'update_state'                     => 'required',
            'update_default'                   => 'required'
        ];
    }
}

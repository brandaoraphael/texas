<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name'          =>  'required',
            'email'         =>  'required|email|unique:admins,email,' . \Auth::guard('admin')->user()->id,
            'username'      =>  'required|min:4|unique:admins,username,' . \Auth::guard('admin')->user()->id,
            'password'      =>  'min:6|confirmed',
            'role'          =>  'required|numeric'
        ];
    }
}

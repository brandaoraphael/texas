<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class AdminUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [

            'name'          =>  'required',
            'email'         =>  'required|email|unique:admins,id,' . $request->segment(2),
            'username'      =>  'required|min:4|unique:admins,id,' . $request->segment(2),
            'password'      =>  'min:6|confirmed',
            'role'          =>  'required|numeric'
        ];
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use App\Config;
use Cart;

class CartMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $delivery_fee = Config::where('key','delivery_fee')->first()->value;
        $minimum_for_free_delivery = Config::where('key','minimum_for_free_delivery')->first()->value;

        $cart_total = Cart::total();

        $packing_fee = 0;

        if($cart_total > 0 && Config::where('key','packing_fee')->first()->value > 0 && Config::where('key','packing_price')->first()->value > 0){

            $packing_fee = ceil(floatval($cart_total) / floatval(Config::where('key','packing_fee')->first()->value)) * floatval(Config::where('key','packing_price')->first()->value);
        }


        if($cart_total >= floatval($minimum_for_free_delivery)){

            $delivery_fee = 0;
        }

        if(Cart::count() > 0){

            $total = $cart_total + $packing_fee + $delivery_fee;
        }

        else{

            $total = 0;
        }

        $request->merge([

            'total' => $total,
            'packing_fee' => $packing_fee,
            'delivery_fee' => $delivery_fee
        ]);

        return $next($request);
    }
}

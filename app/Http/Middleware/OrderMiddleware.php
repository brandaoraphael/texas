<?php

namespace App\Http\Middleware;

use App\Order;
use Closure;

class OrderMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $orders = Order::whereIn('status',[0,1])->get();

        $request->merge(['orders' => $orders]);

        return $next($request);
    }
}

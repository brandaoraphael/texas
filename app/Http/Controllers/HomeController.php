<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Instagram;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $response     = Instagram::users()->getMedia('self');
        $instagrams   =  $response->get();

        return view('home')
            ->with('instagrams',$instagrams);
    }
}

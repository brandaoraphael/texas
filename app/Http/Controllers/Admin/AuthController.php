<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AuthLoginRequest;
use Auth;

class AuthController extends Controller
{
    protected $redirectTo = '/admin';
    protected $guard = 'admin';

    public function __construct()
    {
    }

    public function index(){

        if(Auth::guard('admin')->check()){

            return redirect('admin');
        }

        return view('admin.auth.login');
    }

    public function login(AuthLoginRequest $request){

        if (Auth::guard('admin')->attempt(['username' => $request->input('username'), 'password' => $request->input('password')])) {

            $admin = Admin::where('username',$request->input('username'))->first();
            Auth::guard('admin')->setUser($admin);
            return redirect()->route('admin.index');
        }

        return redirect()->route('admin.login')->with('error','Usuário ou senha inválidos');
    }
//
//    public function resetPassword()
//    {
//        return view('admin.auth.passwords.email');
//    }
//
    public function logout(){

        Auth::guard('admin')->logout();
        return redirect('/admin/login');
    }
}
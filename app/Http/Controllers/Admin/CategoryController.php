<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryStoreRequest;
use App\Http\Requests\Admin\CategoryUpdateRequest;

class CategoryController extends Controller
{

    private $category_service;

    public function __construct(){

        $this->category_service = new CategoryService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $scripts = [

            asset('assets/admin/js/page/category.js')
        ];

        $data = $request->all();
        $categories = $this->category_service->get_categories($data);

        return view('admin.category.index')
            ->with('categories',$categories)
            ->with('title','Categorias')
            ->with('scripts',$scripts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {

        $data =  $request->all();

        $category_service_store_response = $this->category_service->store($data);

        if($category_service_store_response['error']){

            return redirect()->back()->with('error',$category_service_store_response['msg']);
        }

        return redirect()->route('admin.category.index')->with('success',$category_service_store_response['msg']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $scripts = [

            asset('assets/admin/js/page/admin_edit.js')
        ];

        $category = Category::find($id);

        if(!$category){

            return redirect()->back()->with('error','Categoria não encontrada');
        }

        return view('admin.category.edit')
            ->with('category',$category)
            ->with('scripts',$scripts);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, $id)
    {
        $data =  $request->all();

        $category_update_response = $this->category_service->category_update($data,$id);

        if($category_update_response['error']){

            return redirect()->back()->with('error',$category_update_response['msg']);
        }

        return redirect()->back()->with('success',$category_update_response['msg']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $category_destroy_response = $this->category_service->category_destroy($id);

        if($category_destroy_response['error']){

            return redirect()->back()->with('error',$category_destroy_response['msg']);
        }

        return redirect()->route('admin.category.index')->with('success',$category_destroy_response['msg']);
    }
}

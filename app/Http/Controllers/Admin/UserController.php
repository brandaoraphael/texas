<?php

namespace App\Http\Controllers\Admin;

use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    private $user_service;

    public function __construct()
    {

        $this->user_service = new UserService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        $scripts = [

            asset('assets/admin/js/page/user.js')
        ];

        $data = $request->all();
        $users = $this->user_service->get_users($data);

        return view('admin.user.index')
            ->with('users',$users)
            ->with('title', 'Usuários')
            ->with('scripts',$scripts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $scripts = [

            asset('assets/plugins/jquery-mask/jquery.mask.min.js'),
            asset('assets/admin/js/page/user.create.js')
        ];

        $states = \Helper::states();

        return view('admin.user.create')
            ->with('scripts',$scripts)
            ->with('states',$states);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{

            $validator = Validator::make($request->all(), [

                'name'                      => 'required|min:5|max:100',
                'email'                     => 'required|email|unique:users',
                'phone'                     => 'required|min:14|max:15',
                'zipcode'                   => 'required|min:10|max:10',
                'district'                  => 'required|max:50',
                'address'                   => 'required|max:100',
                'number'                    => 'required|max:20',
                'complement'                => 'max:50',
                'city'                      => 'required|max:100',
                'state'                     => 'required'

            ]);

            if ($validator->fails()) {

                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $data = $request->all();
            $data['password'] = \Helper::generate_password();
            $user_store_response = $this->user_service->store($data);

            if($user_store_response['error']){

                return redirect()->back()->with('error',$user_store_response['msg']);
            }

            return redirect()->route('admin.user.index');
        }
        catch (Exception $e){

            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $states = \Helper::states();

        $scripts = [

            asset('assets/plugins/jquery-mask/jquery.mask.min.js'),
            asset('assets/admin/js/page/user.edit.js')
        ];

        $user = User::find($id);

        if(!$user){

            return redirect()->back()->with('error','Usuário não encontrado');
        }

        return view('admin.user.edit')
            ->with('user',$user)
            ->with('states',$states)
            ->with('scripts',$scripts);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=emails-" . time() . ".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $callback = $this->user_service->export();
        return response()->stream($callback, 200, $headers);
    }
}

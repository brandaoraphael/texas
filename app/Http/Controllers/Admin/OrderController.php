<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Order;
use App\OrderItem;
use App\Product;
use App\Services\OrderService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{

    private $order_service;

    public function __construct()
    {

        $this->order_service = new OrderService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $scripts = [

            asset('assets/admin/js/page/order.js')
        ];

        $data = $request->all();
        $orders = $this->order_service->get_orders($data);

        $order_status_array = [

            0 => 'Pedidos pendentes',
            1 => 'Em trânsito',
            2 => 'Entregue',
            3 => 'Cancelado'
        ];

        if(isset($data['status']) && array_key_exists($data['status'],$order_status_array)){

            $title = $order_status_array[$data['status']];
        }

        return view('admin.order.index')
            ->with('orders',$orders)
            ->with('title',isset($title) ? $title : 'Pedidos')
            ->with('scripts',$scripts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $scripts = [

            asset('assets/plugins/print/jQuery.print.js'),
            asset('assets/admin/js/page/order_show.js')
        ];

        $order = Order::find($id);

        $categories = Category::select('categories.*')
            ->leftJoin('products','products.category_id','=','categories.id')
            ->leftJoin('order_items','order_items.product_id','=','products.id')
            ->where('order_items.order_id',$id)
            ->groupBy('id')
            ->get();

        $order_items = OrderItem::where('order_id',$id)
            ->get();

        $dados_empresa = \Config::get('daikon.dados_empresa');

        return view('admin.order.show')
            ->with('order',         $order)
            ->with('order_items',   $order_items)
            ->with('categories',    $categories)
            ->with('dados_empresa', $dados_empresa)
            ->with('scripts',       $scripts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();
        $data['id'] = $id;

        $update_order_response = $this->order_service->update_order($data);

        if($update_order_response['error']){

            return redirect()->back()->with('error',$update_order_response['msg']);
        }

        return redirect()->back()->with('success',$update_order_response['msg']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function check(){

        return Order::where('status',0)->count();
    }

    public function update_order_status($order_id,$status){

        $data = [

            'id'        => $order_id,
            'status'    => $status
        ];

        $update_order_status_response = $this->order_service->update_order($data);

        if($update_order_status_response['error']){

            return redirect()->back()->with('error',$update_order_status_response['msg']);
        }

        return redirect()->back()->with('success',$update_order_status_response['msg']);
    }
}

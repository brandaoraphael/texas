<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductStoreRequest;
use App\Http\Requests\Admin\ProductUpdateRequest;

class ProductController extends Controller
{

    private $product_service;

    public function __construct(){

        $this->product_service = new ProductService();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $scripts = [

            asset('assets/admin/js/page/product.js')
        ];

        $data = $request->all();
        $products = $this->product_service->get_products($data);

        return view('admin.product.index')
            ->with('products',$products)
            ->with('scripts',$scripts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $scripts = [

            asset('assets/plugins/jquery-mask/jquery.mask.min.js'),
            asset('assets/admin/js/page/product_create.js')
        ];

        $categories = Category::pluck('name','id');

        return view('admin.product.create')
            ->with('scripts',$scripts)
            ->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {

        $data = $request->all();
        $product_store_response = $this->product_service->product_store($data);

        if($product_store_response['error']){

            return redirect()->back()->with('error',$product_store_response['msg']);
        }

        return redirect()->route('admin.product.index')->with('success',$product_store_response['msg']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $scripts = [

            asset('assets/plugins/jquery-mask/jquery.mask.min.js'),
            asset('assets/admin/js/page/product_update.js')
        ];

        $product = Product::find($id);

        $categories = Category::pluck('name','id');

        return view('admin.product.edit')
            ->with('product',$product)
            ->with('categories',$categories)
            ->with('scripts',$scripts);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        $data = $request->all();
        $product_update_response = $this->product_service->product_update($data,$id);

        if($product_update_response['error']){

            return redirect()->back()->with('error',$product_update_response['msg']);
        }

        return redirect()->route('admin.product.index')->with('success',$product_update_response['msg']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $product_destroy_response = $this->product_service->product_destroy($id);

        if($product_destroy_response['error']){

            return redirect()->back()->with('error',$product_destroy_response['msg']);
        }

        return redirect()->route('admin.product.index')->with('success',$product_destroy_response['msg']);
    }
}

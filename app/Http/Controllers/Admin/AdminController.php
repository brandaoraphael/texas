<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Services\AdminService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\Admin\AdminUpdateRequest;
use App\Http\Requests\Admin\ProfileUpdateRequest;
use App\Http\Requests\Admin\AdminStoreRequest;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    private $admin_service;

    public function __construct()
    {
        $this->admin_service = new AdminService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {



        $data = $request->all();
        $admins = $this->admin_service->get_admins($data);

        return view('admin.admin.index')
            ->with('admins',$admins);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminStoreRequest $request)
    {

        $data = $request->all();

        $admin_store_response = $this->admin_service->admin_store($data);

        if($admin_store_response['error']){

            return redirect()->back()->with('error',$admin_store_response['msg']);
        }

        return redirect()->route('admin.admin.index')->with('success',$admin_store_response['msg']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $scripts = [

            asset('assets/admin/js/page/admin_edit.js')
        ];

        $admin = Admin::find($id);

        if(!$admin){

            return redirect()
                ->back()
                ->with('error', 'Atendente não encontrado');
        }

        return view('admin.admin.edit')
            ->with('admin',$admin)
            ->with('scripts',$scripts);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUpdateRequest $request, $id)
    {

        $data = $request->all();

        $admin_update_response = $this->admin_service->admin_update($data,$id);

        if($admin_update_response['error']){

            return redirect()->back()->with('error',$admin_update_response['msg']);
        }

        return redirect()->back()->with('success',$admin_update_response['msg']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin_destroy_response = $this->admin_service->admin_destroy($id);

        if($admin_destroy_response['error']){

            return redirect()->back()->with('error',$admin_destroy_response['msg']);
        }

        return redirect()->route('admin.admin.index')->with('success',$admin_destroy_response['msg']);
    }

    public function profile(){

        $admin = Admin::find(\Auth::guard('admin')->user()->id);

        if(!$admin){

            return redirect()
                ->back()
                ->with('error', 'Atendente não encontrado');
        }

        return view('admin.admin.profile')
            ->with('admin',$admin);
    }

    public function update_profile(ProfileUpdateRequest $request){

        $admin = \Auth::guard('admin')->user();

        $data = $request->all();

        $data['role'] = $admin->role;

        $admin_update_response = $this->admin_service->admin_update($data,$admin->id);

        if($admin_update_response['error']){

            return redirect()->back()->with('error',$admin_update_response['msg']);
        }

        return redirect()->back()->with('success',$admin_update_response['msg']);
    }
}

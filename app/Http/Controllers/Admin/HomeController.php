<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Services\OrderService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    private $order_service;

    public function __construct()
    {
        $this->order_service = new OrderService();
    }

    public function index(Request $request){

        $scripts = [

            asset('assets/bower_components/jquery-confirm/jquery.confirm.min.js'),
            asset('assets/admin/js/page/order.js')
        ];

        $orders = $this->order_service->get_orders(['status' => [0,1],'days' => 1],'desc');

        return view('admin.index')
            ->with('orders',$orders)
            ->with('title',isset($title) ? $title : 'Últimos Pedidos')
            ->with('scripts',$scripts);
    }
}

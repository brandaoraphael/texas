<?php

namespace App\Http\Controllers\Admin;

use App\Config;
use App\Services\ConfigService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\Admin\UpdateConfigRequest;
use App\Http\Controllers\Controller;

class DeliveryController extends Controller
{

    private $config_service;

    public function __construct()
    {

        $this->config_service = new ConfigService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $scripts = [

            asset('assets/plugins/jquery-mask/jquery.mask.min.js'),
            asset('assets/admin/js/page/update_config.js')
        ];

        $configs = Config::all();

        return view('admin.delivery.index')
            ->with('configs',$configs)
            ->with('scripts',$scripts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateConfigRequest $request)
    {
        $data = $request->all();
        $update_config_response = $this->config_service->update_config($data);

        if($update_config_response['error']){

            return redirect()->back()->with('error',$update_config_response['msg']);
        }

        return redirect()->back()->with('success',$update_config_response['msg']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

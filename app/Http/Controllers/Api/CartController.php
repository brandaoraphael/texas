<?php

namespace App\Http\Controllers\Api;

use App\Services\CartService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\Api\CartUpdateRequest;
use App\Http\Controllers\Controller;
class CartController extends Controller
{

    private $cart_service;

    public function __construct()
    {
        $this->cart_service = new CartService();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return 'Oi';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add(CartUpdateRequest $request)
    {
        $data = $request->all();
        return json_encode($this->cart_service->cart_add($data));
    }

    public function del(CartUpdateRequest $request)
    {
        $data = $request->all();
        return json_encode($this->cart_service->cart_del($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Config;
use App\Order;
use App\Services\OrderService;
use App\UserAddress;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\OrderStoreRequest;
use \Cart;
use Auth;

class OrderController extends Controller
{

    private $order_service;

    public function __construct()
    {

        $this->order_service = new OrderService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(!Cart::count()){

            return redirect()->route('index')
                ->with('error','Você não possui pedidos');
        }

        $scripts = [

            asset('assets/plugins/jquery-mask/jquery.mask.min.js'),
            asset('assets/js/page/cart.js')
        ];

        $delivery_fee = Config::where('key','delivery_fee')->first()->value;
        $minimum_for_free_delivery = Config::where('key','minimum_for_free_delivery')->first()->value;


        $cart_total = Cart::total();

        if($cart_total >= floatval($minimum_for_free_delivery)){

            $delivery_fee = 0;
        }

        $packing_fee = 0;

        $config_packing_fee = Config::where('key','packing_fee')->first()->value;
        $config_packing_price = floatval(Config::where('key','packing_fee')->first()->value);

        if($config_packing_fee > 0 && $config_packing_price > 0)
            $packing_fee = ceil(floatval($cart_total) / $config_packing_fee) * $config_packing_price;

        $payment_modes = \Config::get('daikon.payment_modes');

        $user = Auth::user();

        $user_addresses = UserAddress::where('user_id',$user->id)
            ->get();

        $total = Cart::total() + $delivery_fee + $packing_fee;

        return view('order.index')
            ->with('delivery_fee',$delivery_fee)
            ->with('packing_fee',$packing_fee)
            ->with('total',$total)
            ->with('scripts',$scripts)
            ->with('payment_modes',$payment_modes)
            ->with('user_addresses',$user_addresses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderStoreRequest $request)
    {

        $dias_funcionamento = \Config::get('daikon.dias_funcionamento');
        $hora_funcionamento = \Config::get('daikon.hora_funcionamento');

        if($request['payment_mode'] == 0 && $request['troco'] == ''){
            return redirect()->back()->with('error','Informe o valor para troco');
        }

        if(!in_array(date('N'),$dias_funcionamento) || date('H:i') < $hora_funcionamento['inicio'] || date('H:i') > $hora_funcionamento['fim']){

            return redirect()->route('index')->with('error','Fora do horário de funcionamento');
        }

        if(!Cart::count()){

            return redirect()->route('index')
                ->with('error','Você não possui itens no seu pedido');
        }

        $data = $request->all();

        $order_store_response = $this->order_service->order_store($data);

        if($order_store_response['error']){

            return redirect()->back()->with('error',$order_store_response['msg']);
        }

        return redirect()->route('order.show',$order_store_response['dados']->id)->with('success',$order_store_response['msg']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();

        if(!$user){

            return redirect()->route('index')->with('error','Não autorizado');
        }

        $order = Order::where('user_id',$user->id)
            ->where('id',$id)
            ->first();

        if(!$order){

            return redirect()->route('index')->with('error','Pedido não localizado');
        }

        $user_address = UserAddress::find($order->user_address_id);

        return view('order.checkout')
            ->with('user',$user)
            ->with('order',$order)
            ->with('user_address',$user_address);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function history(){

        $user = Auth::user();

        if(!$user){

            return redirect()->route('index')->with('error','Não autorizado');
        }

        $orders = Order::where('user_id',$user->id)
            ->orderBy('id','desc')
            ->get();


        return view('order.history')
            ->with('user',$user)
            ->with('orders',$orders);
    }
}

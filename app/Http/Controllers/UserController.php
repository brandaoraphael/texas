<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserAddressStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Services\UserService;
use Auth;

class UserController extends Controller
{

    private $user_service;

    public function __construct(){

        $this->user_service =  new UserService();
    }

    public function login(){

        $scripts = [

            asset('assets/plugins/jquery-mask/jquery.mask.min.js'),
            asset('assets/js/page/siginup.js')
        ];

        $states = \Helper::states();

        return View::make('user.login')
            ->with('states',$states)
            ->with('scripts',$scripts);
    }

    public function verify(Request $request){

        $validator = Validator::make($request->all(), [

            'email'     => 'required|email',
            'password'  => 'required|min:6|max:50'
        ]);

        if ($validator->fails()) {

            return redirect()
                ->back()
                ->withErrors($validator, 'login')
                ->withInput();
        }

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {

            return redirect()->route('product.index');
        }

        return redirect()->route('user.login.form')->with('error','Usuário ou senha inválidos');
    }

    public function store(Request $request){

        try{

            $validator = Validator::make($request->all(), [

                'name'                      => 'required|min:5|max:100',
                'email'                     => 'required|email|unique:users',
                'phone'                     => 'required|min:14|max:15',
                'password'                  => 'required|min:6|max:50|confirmed',
                'password_confirmation'     => 'required|min:6|max:50',
                'zipcode'                   => 'required|min:10|max:10',
                'district'                  => 'required|max:50',
                'address'                   => 'required|max:100',
                'number'                    => 'required|max:20',
                'complement'                => 'max:50',
                'city'                      => 'required|max:100',
                'state'                     => 'required'

            ]);

            if ($validator->fails()) {

                return redirect()
                    ->back()
                    ->withErrors($validator, 'signup')
                    ->withInput();
            }

            $data = $request->all();
            $user_store_response = $this->user_service->store($data);

            if($user_store_response['error']){

                return redirect()->back()->with('signup_error',$user_store_response['msg']);
            }

            return redirect()->route('product.index');
        }
        catch (Exception $e){

            return redirect()->back()->with('signup_error',$e->getMessage());
        }
    }

    public function logout(){

        Auth::logout();
        return redirect()->route('index');
    }

    public function profile(){


        if(!Auth::check()){

            return redirect()->route('index')->with('error','Você precisa estar logado');
        }

        $scripts = [

            asset('assets/plugins/jquery-mask/jquery.mask.min.js'),
            asset('assets/js/page/update_profile.js')
        ];

        $states = \Helper::states();

        return View::make('user.edit')
            ->with('user',Auth::user())
            ->with('addresses', Auth::user()->addresses)
            ->with('states',$states)
            ->with('scripts',$scripts);
    }

    public function address(UserAddressStoreRequest $request){

        if(!Auth::check()){

            return redirect()->route('home')->with('error','Você precisa estar logado');
        }

        try{

            $data = $request->all();

            $data['user_id'] = Auth::user()->id;

            $user_service_store_address_response = $this->user_service->store_address($data);

            if($user_service_store_address_response['error']){

                return redirect()->back()->with('error',$user_service_store_address_response['msg']);
            }

            return redirect()->back()->with('success',$user_service_store_address_response['msg']);

        }
        catch(Exception $e){

            return redirect()->back()->with('error',$e->getMessage());
        }

    }

    public function update(UserUpdateRequest $request){

        if(!Auth::check()){

            return redirect()->route('index')->with('error','Você precisa estar logado');
        }

        try{

            $data = $request->all();

            $data['id'] = Auth::user()->id;

            $user_service_update_response = $this->user_service->update($data);

            if($user_service_update_response['error']){

                return redirect()->back()->with('error',$user_service_update_response['msg']);
            }

            return redirect()->back()->with('success',$user_service_update_response['msg']);

        }
        catch(Exception $e){

            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function password(){

        return View::make('user.password');
    }
}

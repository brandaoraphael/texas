<?php

namespace App\Http;

class Helper
{

    public static function states()
    {
        return [

            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins'
        ];
    }

    static function format_br_zipcode($zipcode){

        $zipcode = preg_replace("/[^0-9]/", "", $zipcode);

        $zip_1 = substr($zipcode, 0, 5);
        $zip_2 = substr($zipcode,-3);

        return $zip_1 . '-' . $zip_2;
    }

    public static function numbers_only($str){

        return preg_replace("/[^0-9]/", "", $str);
    }

    public static function remove_chars($string){

        $string = preg_replace("/[áàâãä]/", "a", $string);
        $string = preg_replace("/[ÁÀÂÃÄ]/", "A", $string);
        $string = preg_replace("/[éèê]/", "e", $string);
        $string = preg_replace("/[ÉÈÊ]/", "E", $string);
        $string = preg_replace("/[íì]/", "i", $string);
        $string = preg_replace("/[ÍÌ]/", "I", $string);
        $string = preg_replace("/[óòôõö]/", "o", $string);
        $string = preg_replace("/[ÓÒÔÕÖ]/", "O", $string);
        $string = preg_replace("/[úùü]/", "u", $string);
        $string = preg_replace("/[ÚÙÜ]/", "U", $string);
        $string = preg_replace("/ç/", "c", $string);
        $string = preg_replace("/Ç/", "C", $string);
        $string = preg_replace("/[][><}{)(:;,!?*%~^`&#@]/", "", $string);
        $string = preg_replace("/ /", "_", $string);

        return $string;
    }

    public static function remove_accents($str){

        return preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $str));
    }

    public static function get_address_correios($zipcode){

        $zipcode = Self::numbers_only($zipcode);

        $content = @file_get_contents('https://viacep.com.br/ws/' .$zipcode. '/json/');

        if(!$content){

            return false;
        }

        return json_decode($content);
    }

    public static function get_order_status_class($status=0){

        $status_class_array = [

            0 => ['class' => 'aguardando',      'text' => 'Aguardando'],
            1 => ['class' => 'saiuparaentrega', 'text' => 'Saiu para entrega'],
            2 => ['class' => 'entregue',        'text' => 'Entregue'],
            3 => ['class' => 'cancelado',       'text' => 'Cancelado']
        ];

        return $status_class_array[$status];
    }

    public static function get_ta(){

        return \Config::get('daikon.payment_options.ta') * 100;
    }

    public static function get_ta_base(){

        return \Config::get('daikon.payment_options.base_ta') * 100;
    }

    public static function calc_tributes($value){

        $base_ta = \Config::get('daikon.payment_options.base_ta');
        return round($value * $base_ta,2);
    }

    public static function generate_password($length = 8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}
$(document).ready(function () {
	if ($(window).width() > 1023) {
		wow = new WOW(
			{
				
			}
		);
		wow.init();
	}

	$('#shopping .product').on('click', function() {
		var $elm = $(".cart-items").addClass("visible");
		$('body').append('<div class="unfocus"> </div>');

		setTimeout(function() {
		  	$elm.removeClass("visible");
		 	$('.unfocus').remove();
		}, 800);
	});

	// Banner alinhamentos
	var bannerHeight = $('.banner').height() - 120;
	$('.banner').css('margin-top', - bannerHeight / 2);

	$('#bt-cart-info-header').on('click', function(){

		if($(this).attr('data-cart-count') > 0){

			$('.cart-items').toggleClass('visible');
		}

	});
	$('.bt-user-info').on('click', function() {
		$('.user-items').toggleClass('visible');
	});

	$('.wrap').find('.order-title').click(function() {
		$(this).next().slideToggle('fast');

		if (!$(this).hasClass('active')) {
			$(this).addClass('active');
		}
		else {
			$(this).removeClass('active');
		}
	});

	$('.navmobile').on('click', function(e) {
		$(this).next('ul').slideToggle('fast');
	    e.stopPropagation();
	})
	$(document).on('click', function (e) {
        $('.navmobile').next('ul').hide();
	});

	// Criando função para o Scroll
	$(document).on("scroll", onScroll);

	// Função rolagem suave das ancoras
	$('a[href^="#"]').on('click', function (e) {
		e.preventDefault();
		$(document).off("scroll");

		$('a').each(function () {
		   $(this).removeClass('ativo');
		})
		$(this).addClass('ativo');

		var target = this.hash,
		   menu = target;
		$target = $(target);
		$('html, body').stop().animate({
		   'scrollTop': $target.offset().top+2
		}, 500, 'swing', function () {
		   window.location.hash = target;
		   $(document).on("scroll", onScroll);
		});
	});

    // Função adicionar class ativo na rolagem (ancoras)
	function onScroll(event){
	    var scrollPos = $(document).scrollTop() + 80;
	    $('header a').each(function () {
	        var currLink = $(this);
	        var refElement = $(currLink.attr("href"));
	        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
		            $('ul li a').removeClass("active");
		            currLink.addClass("active");

		            if (scrollPos < 250) {
		            	$('header').removeClass('small');
		            }
		            else {
		            	$('header').addClass('small');
		            }
	        }

	        else{
	            currLink.removeClass("ativo");
	        }
	    });
	}
});
$(function(){

    var $base_url = $('#base_url').val()

    $(document).on('click', '.bt-addcart, .bt-product-remove, .bt-product-add',function(e){

        e.preventDefault();

        var $element = $(this),
            $url,
            $product_id,
            $raw_id = $element.parent().parent().attr('data-raw-id');

        if($element.hasClass('bt-addcart') || $element.hasClass('bt-product-add')){

            $url = $base_url + '/api/cart/add';
        }

        if($element.hasClass('bt-product-remove')){

            $url = $base_url + '/api/cart/del';
        }

        if($element.parent().find('span').html() == 0){

            return false;
        }

        if($element.hasClass('bt-addcart')){

            $product_id = $element.attr('data-product-id');
        }

        if($element.hasClass('bt-product-remove') || $element.hasClass('bt-product-add')){

            $product_id = $element.parent().parent().attr('data-product-id');
        }

        //console.log($element.data('product-id'));

        $.ajax({

            type: "POST",
            url: $url,
            data: { product_id : $product_id, raw_id : $raw_id },
            dataType: 'json',

            success: function(response) {

                //console.log(response);

                if($element.hasClass('bt-product-remove') && response.dados.qty === 0){

                    $element.parent().parent().remove();
                    if($('#current_url').val() == 'order'){

                        window.location = $base_url;
                    }
                }

                if(!response.error && response.dados){

                    //console.log(response.dados);

                    $element.parent().parent().attr('data-raw-id',response.dados.raw_id);
                    $('.bt-cart-info span').html(response.dados.total_items);
                    $('.cart-items ul').html('');
                    $('.cart-items .taxa span').html('R$ ' + response.dados.delivery_fee);
                    $('.cart-items .subtotal span').html('R$ ' + response.dados.sub_total);

                    $("[data-raw-id='" + response.dados.raw_id + "'] .total-item").html('R$ ' + (response.dados.price * response.dados.qty).toFixed(2).replace(".", ","));
                    $("[data-raw-id='" + response.dados.raw_id + "'] .qtd span").html(response.dados.qty);
                    $('.delivery-fee b').html("R$ " + response.dados.delivery_fee);
                    $('.total span').html("R$ " + response.dados.total);


                    $('#bt-cart-info-header').attr('data-cart-count',response.dados.qty);

                    if(response.dados.qty==0){

                        $('.cart-items').toggleClass('visible');
                    }
                    //console.log($cartElement.attr('data-raw-id'));
                    //$cartElement.find('.total-item').html('R$ ' + (response.dados.price * response.dados.qty).toFixed(2).replace(".", ","));

                    //$('.total-item').html('R$ ' + (response.dados.price * response.dados.qty).toFixed(2).replace(".", ","));

                    //data-raw-id="{{ session()->has('product_raw_id_' . $product->id) ? session()->get('product_raw_id_' . $product->id) : '' }}"

                    $.each(response.dados.cart_all, function (index, value) {

                        var str = "<li data-raw-id='" + value.__raw_id + "' data-product-id='" + value.id + "'>" +
                            "<b>" +
                            "<small class='bt-product-remove ti-minus'> </small>" + value.qty +
                            "<small class='bt-product-add ti-plus'> </small>" +
                            "</b>" +
                            "<strong>" +
                            value.name +
                            "<span>R$ " + value.total.toFixed(2).replace(".", ",") +
                            "</span>" +
                            "</strong>" +
                            "</li>";
                        $('.cart-items ul').append(str);
                    });


                    //$element.parent().find('span').html(response.dados.qty);

                    //$('.header_total_pedido').html("R$ " + response.dados.total + " (" + response.dados.total_items + ")");

                    // if(response.dados.total_items == 0){
                    //
                    //     $('.finalizar').addClass('hide');
                    //     $('.btn-cardapio').removeClass('hide');
                    //     $('.btn-carrinho').addClass('hide');
                    // }
                    //
                    // else{
                    //
                    //     $('.btn-carrinho').removeClass('hide');
                    //     $('.btn-cardapio').addClass('hide');
                    //     $('.finalizar').removeClass('hide');
                    // }
                }
            },

            fail: function(){

                console.log('Erro');
            }
        });
    });


    /*

    $(document).on('click', '.bt-produto-add,.bt-produto-remover',function(e){

        e.preventDefault();

        var element = $(this);

        if(element.attr('class') == 'bt-produto-add'){

            var url = base_url + '/api/cart/add';
        }

        else{

            var url = base_url + '/api/cart/del';

            if(element.parent().find('span').html() == 0){

                return false;
            }
        }

        var raw_id = element.parent().parent().attr('data-raw_id');

        $.ajax({

            type: "POST",
            url: url,
            data: { product_id : element.data('product_id'), raw_id : raw_id },
            dataType: 'json',

            success: function(response) {

                if(!response.error && response.dados){

                    element.parent().parent().attr('data-raw_id',response.dados.raw_id);
                    element.parent().find('span').html(response.dados.qty);

                    $('.header_total_pedido').html("R$ " + response.dados.total + " (" + response.dados.total_items + ")");

                    if(response.dados.total_items == 0){

                        $('.finalizar').addClass('hide');
                        $('.btn-cardapio').removeClass('hide');
                        $('.btn-carrinho').addClass('hide');
                    }

                    else{

                        $('.btn-carrinho').removeClass('hide');
                        $('.btn-cardapio').addClass('hide');
                        $('.finalizar').removeClass('hide');
                    }
                }
            },

            fail: function(){

                console.log('Erro');
            }
        });
    });
    */
});
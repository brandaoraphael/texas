$(document).ready(function(){

    $('.zipcode').mask('99.999-999');
    $('.date').mask('99/99/9999');

    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    $('.phone').mask(SPMaskBehavior, spOptions);

    //Quando o campo cep perde o foco.
    $(".zipcode").blur(function () {


        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');
        var el_pai = $(this).parent().parent();
        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {



                //Preenche os campos com "..." enquanto consulta webservice.
                //el_pai.find('.address').val("...");
                //el_pai.find('.district').val("...");
                //el_pai.find('.city').val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                    if (!("erro" in dados)) {

                        if(dados.localidade == 'Campina Grande'){

                            //Atualiza os campos com os valores da consulta.
                            el_pai.find('.address').val(dados.logradouro);
                            el_pai.find('.district').val(dados.bairro);
                            el_pai.find('.city').val(dados.localidade);
                            el_pai.find('.state').val(dados.uf);
                        }
                        else{

                            limpa_formulário_cep(el_pai);
                            alert("A cidade indicada não possui cobertura de entrega");
                        }
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep(el_pai);
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep(el_pai);
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep(el_pai);
        }
    });
});

function limpa_formulário_cep(el_pai) {

    el_pai.find('.zipcode').val("");
    el_pai.find('.address').val("");
    el_pai.find('.district').val("");
    el_pai.find('.city').val("");
    el_pai.find('.state').val("");
}
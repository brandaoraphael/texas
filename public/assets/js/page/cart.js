$(function(){

    $('.money').mask('000000000000000.00', {reverse: true});

    $('select[name=payment_mode]').on('change',function(){

        if($(this).val() == 0)
            $('input[name=troco]').val('').prop('disabled',false)
                .prop('required',true);
        else
            $('input[name=troco]').val('').prop('disabled',true)
                .prop('required',false);
    });

    var base_url = $('#base_url').val();

    $(document).on('click', '.bt-produto-add,.bt-produto-remover',function(e){

        e.preventDefault();

        var element = $(this);

        if(element.attr('class') == 'bt-produto-add'){

            var url = base_url + '/api/cart/add';
        }

        else{

            var url = base_url + '/api/cart/del';
        }

        var raw_id = element.parent().parent().attr('data-raw_id');
        var product_id = element.parent().parent().attr('data-product_id');

        $.ajax({

            type: "POST",
            url: url,
            data: { product_id : product_id, raw_id : raw_id },
            dataType: 'json',

            success: function(response) {

                if(!response.error && response.dados){

                    element.parent().parent().attr('data-raw_id',response.dados.raw_id);
                    element.parent().parent().find('.qtd').html(response.dados.qty);
                    element.parent().parent().find('.total_item').html("R$ " + response.dados.sub_total);

                    $('.header_total_pedido').html("R$ " + response.dados.total + " (" + response.dados.total_items + ")");
                    $('.cart_total').html("R$ " + response.dados.total);
                    $('.delivery_fee').html("R$ " + response.dados.delivery_fee);
                    $('.packing_fee').html("R$ " + response.dados.packing_fee);

                    if(response.dados.qty == 0){

                        element.parent().parent().remove();
                    }

                    if(response.dados.total_items == 0){

                        $('.finalizar').addClass('hide');
                        $('.btn-cardapio').removeClass('hide');
                        $('.btn-carrinho').addClass('hide');
                        window.location.replace($('#base_url').val());
                    }

                    else{

                        $('.btn-carrinho').removeClass('hide');
                        $('.btn-cardapio').addClass('hide');
                        $('.finalizar').removeClass('hide');
                    }
                }
            },

            fail: function(){

                console.log('Erro');
            }
        });
    });
});
var base_url = $('#base_url').val();

$(document).ready(function () {

	$('.userbox').on('click', function() {
		$(this).next('ul').toggle();
	});

	$('tr[data-href]').on("click", function(e) {

		if(!$(e.target).hasClass('confirm'))
			document.location = $(this).data('href');
	});

	var previewWidth = $('#imgpreview').width();
	$('#imgpreview').css('height', previewWidth);

	var productImg = $('#imgpreview img');
	$.each(productImg, function(index, item){

		var productImgHeight = $(item).height();

	   $(item).css('margin-top', - productImgHeight / 2);
	});


    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        
        $(this).parent().next('span').html(fileName);
        readURL(this);
    });

    function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#imgpreview img').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	Notification.requestPermission(function(perm) {
		console.log(perm);
	});

	setInterval(function() {

		var url = base_url + '/admin/order/check';

		$.ajax({

			type: "POST",
			url: url,
			dataType: 'json',

			success: function (response) {

				console.log(response);

				if($('.new_orders').html() < response){

					var notification = new Notification("NOVO PEDIDO", {
						dir: "auto",
						icon: base_url + '/assets/admin/img/logo.png',
						lang: "",
						body: "Você tem " + response + " novos pedidos",
						tag: "tag",
					});

					$('.new_orders').html(response);

					notification.onclick = function () {

						window.location.replace(base_url + '/admin');
					}

					$.playSound(base_url + '/assets/sounds/alert');
				}
			},

			fail: function () {

				console.log('Erro');
			}
		});

	}, 3000);

	$('.cancel,.close').on('click',function(){

		$('.confirmation-modal').removeClass('fadeIn');
	});

	$(document).on('keyup',function(e){

		if(e.keyCode == 27){
			$('.confirmation-modal').removeClass('fadeIn');
		}
	});

	var productList = [
		{
			value: "Sushi de Salmão",
			price: "R$ 1.80"
		},
		{
			value: "Sushi de Kani",
			price: "R$ 1.50"
		},
		{
			value: "Sushi Qualquer",
			price: "R$ 3.00"
		}
    ];
    $( ".autocomplete-products" ).autocomplete({
    	minLength: 3,
    	source: productList,
    	focus: function( event, ui ) {
	        // $( ".autocomplete" ).val( ui.item.value );
	        return false;
	    },
	   	select: function( event, ui ) {
	        $(this).val("");
	        $(".shopping-cart.items").prepend('<tr><td class="align-top"><strong>' + ui.item.value + ' </strong></td>ßß<td class="text-center align-top"><div class="item-quantity"><b class="bt-product-add">-</b><span>1</span><b class="bt-product-add">+</b></div></td><td class="text-right align-top">'+ ui.item.price +'</td></tr>');
	 		
	        return false;
	    }
    });

    var customerList = [
		"Raphael Brandão",
		"Raphael Limão",
		"Raphael Limão",
		"Raphael Limão",
		"Raphael Limão",
		"Raphael Limão"
    ];
    $( ".autocomplete-customers" ).autocomplete({
    	minLength: 3,
    	source: customerList,
    	focus: function( event, ui ) {
	        // $( ".autocomplete" ).val( ui.item.value );
	        return false;
	    }
    });

});


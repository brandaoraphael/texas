$(function(){

    $('.delete').on('click',function(e){

        var el = $(this);

        e.preventDefault();

        $('.confirmation-modal').addClass('fadeIn');

        $('.confirm.btn').on('click',function(){
            el.parent('form').submit();
        });
    });
});
$(function(){

    $('.imprimir').on('click',function(e){

        e.preventDefault();
        $(this).parent().parent().print({
            globalStyles : true,
            append : '#obs,.address'
        });

        /*

         // Opções

         globalStyles: true,
         mediaPrint: false,
         stylesheet: null,
         noPrintSelector: ".no-print",
         iframe: true,
         append: null,
         prepend: null,
         manuallyCopyFormValues: true,
         deferred: $.Deferred(),
         timeout: 750,
         title: null,
         doctype: '<!doctype html>'

         */
    });

    $('.print-view').on('click',function(e){

        var el = $(this);

        e.preventDefault();

        $('.confirmation-modal').addClass('fadeIn');

        $('.confirm.btn').on('click',function(){
            el.parent('form').submit();
        });
    });

    $('.print-receipt').on('click',function(){

        $('#recibo').parent().print({

            globalStyles : true
        });
    });

    $('.print-command').on('click',function(){

        $('#command').print({
            globalStyles : true,
            append : '  .address'
        });
    });
});
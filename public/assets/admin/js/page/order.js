$(function(e){

    $('select[name=interval]').on('change',function(){

        $('form[name=interval_form]').submit();
    });

    $('.confirm').on('click',function(e){

        e.preventDefault();

        $('.confirmation-modal').addClass('fadeIn');
        $('.confirm.btn').attr('data-href',$(this).data('href'));
        $('.modal-title').html('Confirmação');
        $('.modal-body').html('Deseja marcar o pedido como entregue?');
    });

    $('.confirm.btn').on('click',function(){

        document.location = $(this).data('href');
    });

    $('.cancel,.close').on('click',function(){

        $('.confirmation-modal').removeClass('fadeIn');
    });

    $(document).on('keyup',function(e){

        if(e.keyCode == 27){

            $('.confirmation-modal').removeClass('fadeIn');
        }
    });
});




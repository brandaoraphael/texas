$(function(){

    $('.money').mask('000000000000000.00', {reverse: true});

    $('select[name=interval]').on('change',function(){

        $('form[name=interval_form]').submit();
    });

    $('.delete').on('click',function(e){

        var el = $(this);

        e.preventDefault();

        $('.confirmation-modal').addClass('fadeIn');
        $('.modal-title').html('Confirmação');
        $('.modal-body').html('Tem certeza que deseja excluir o produto?');

        $('.confirm.btn').on('click',function(){
            console.log($(this).parent());
            el.parent().parent('form').submit();
        });
    });

    $('.cancel,.close').on('click',function(){

        $('.confirmation-modal').removeClass('fadeIn');
    });

    $(document).on('keyup',function(e){

        if(e.keyCode == 27){

            $('.confirmation-modal').removeClass('fadeIn');
        }
    });
});
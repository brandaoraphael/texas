<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::get('/',             ['as' => 'home',        'uses' => 'HomeController@index']);
//Route::get('/login',        ['as' => 'login',       'uses' => 'LoginController@index']);
//Route::get('/menu',         ['as' => 'menu',        'uses' => 'MenuController@index']);
//Route::get('/order',        ['as' => 'order',       'uses' => 'OrderController@index']);
//Route::get('/orders',       ['as' => 'orders',      'uses' => 'OrderController@orders']);
//Route::get('/user',         ['as' => 'user',        'uses' => 'UserController@orders']);


// Rotas da Api

Route::group(['prefix' => 'api','as' => 'api.','middleware' => ['api']],function(){

    Route::group(['prefix' => 'cart','as' => 'cart.'],function(){

        Route::post('add',              ['uses' => 'Api\CartController@add',            'as' => 'add']);
        Route::post('del',              ['uses' => 'Api\CartController@del',            'as' => 'del']);
    });
});

// Rotas do Site

Route::group(['middleware' => ['cart']],function(){

    Route::get('/',['uses' => 'HomeController@index', 'as' => 'index']);

    Route::get('/home',function(){

        return redirect()->route('index');
    });

    Route::get('/login',function(){

        return redirect()->route('user.login.form');
    });

    Route::get('/logout',function(){

        return redirect()->route('user.logout');
    });

    // Products

    Route::group(['prefix' => 'product','as' => 'product.'],function(){

        Route::get('/',                     ['uses' => 'ProductController@index',           'as' => 'index']);
    });

    Route::group(['prefix' => 'order','as' => 'order.','middleware' => 'auth'],function(){

        Route::get('/',                     ['uses' => 'OrderController@index',             'as' => 'index']);
        Route::post('store',                ['uses' => 'OrderController@store',             'as' => 'store']);
        Route::get('history',               ['uses' => 'OrderController@history',           'as' => 'history']);
        Route::get('{id}',                  ['uses' => 'OrderController@show',              'as' => 'show']);
    });

    Route::group(['prefix' => 'user','as' => 'user.'],function(){

        Route::group(['prefix' => 'login','as' => 'login.'],function(){

            Route::get('/',                 ['uses' => 'UserController@login',                              'as' => 'form']);
            Route::post('/',                ['uses' => 'UserController@verify',                             'as' => 'verify']);
        });

        Route::post('store',                ['uses' => 'UserController@store',                              'as' => 'store']);
        Route::get('logout',                ['uses' => 'UserController@logout',                             'as' => 'logout']);
        Route::get('profile',               ['uses' => 'UserController@profile',                            'as' => 'profile']);
        Route::post('address',              ['uses' => 'UserController@address',                            'as' => 'add_address']);
        Route::post('{id}/update',          ['uses' => 'UserController@update',                             'as' => 'update']);

//        Route::group(['prefix' => 'user','as' => 'user.'],function(){
//
//            Route::get('siginup',               ['uses' => 'UserController@create',                             'as' => 'create']);
//            Route::post('siginup',              ['uses' => 'UserController@store',                              'as' => 'store']);
//            Route::post('login',                ['uses' => 'UserController@login',                              'as' => 'login']);
//            Route::get('logout',                ['uses' => 'UserController@logout',                             'as' => 'logout']);
//            Route::get('profile',               ['uses' => 'UserController@profile',                            'as' => 'profile']);
//            Route::post('address',              ['uses' => 'UserController@address',                            'as' => 'add_address']);
//            Route::post('{id}/update',          ['uses' => 'UserController@update',                             'as' => 'update']);
//
//        });



//        Route::post('siginup',              ['uses' => 'UserController@store',                              'as' => 'store']);
//
//        Route::get('logout',                ['uses' => 'UserController@logout',                             'as' => 'logout']);
//        Route::get('profile',               ['uses' => 'UserController@profile',                            'as' => 'profile']);
//        Route::post('address',              ['uses' => 'UserController@address',                            'as' => 'add_address']);
//        Route::post('{id}/update',          ['uses' => 'UserController@update',                             'as' => 'update']);

    });

    // Auth Routes

    Route::group(['prefix' => 'password','as' => 'password.'],function(){

        Route::get('email',                 ['uses' => 'Auth\ForgotPasswordController@showLinkRequestForm', 'as' => 'email']);
        Route::post('email',                ['uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail',  'as' => 'email']);
        Route::get('reset/{token}',         ['uses' => 'Auth\ResetPasswordController@showResetForm',        'as' => 'reset']);
        Route::post('reset',                ['uses' => 'Auth\ResetPasswordController@reset',                'as' => 'reset']);
    });

});

// Rotas do Admin

Route::group(['prefix' => 'admin','as' => 'admin.'],function(){

    // Rotas de Login

    Route::get('login',                 ['uses' => 'Admin\AuthController@index',            'as' => 'login']);
    Route::post('login',                ['uses' => 'Admin\AuthController@login',            'as' => 'login']);


    Route::group(['middleware' => ['orders','admin']],function(){

        // Logout

        Route::get('logout',                ['uses' => 'Admin\AuthController@logout',               'as' => 'logout']);

        // Profile

        Route::get('profile',               ['uses' => 'Admin\AdminController@profile',             'as' => 'profile']);
        Route::post('profile',              ['uses' => 'Admin\AdminController@update_profile',      'as' => 'profile']);

        // Rota Principal

        Route::get('',                      ['uses' => 'Admin\HomeController@index',        'as' => 'index']);

        // Rota Produtos

        Route::group(['prefix' => 'product','as' => 'product.'],function(){

            Route::get('/',                 ['uses' => 'Admin\ProductController@index',         'as' => 'index']);
            Route::get('create',            ['uses' => 'Admin\ProductController@create',        'as' => 'create']);
            Route::post('store',            ['uses' => 'Admin\ProductController@store',         'as' => 'store']);
            Route::get('{id}/edit',         ['uses' => 'Admin\ProductController@edit',          'as' => 'edit']);
            Route::put('{id}',              ['uses' => 'Admin\ProductController@update',        'as' => 'update']);
            Route::delete('{id}',           ['uses' => 'Admin\ProductController@destroy',       'as' => 'destroy']);
        });

        // Rota Categorias

        Route::group(['prefix' => 'category','as' => 'category.'],function(){

            Route::get('/',                 ['uses' => 'Admin\CategoryController@index',    'as' => 'index']);
            Route::get('create',            ['uses' => 'Admin\CategoryController@create',   'as' => 'create']);
            Route::post('store',            ['uses' => 'Admin\CategoryController@store',    'as' => 'store']);
            Route::get('{id}/edit',         ['uses' => 'Admin\CategoryController@edit',     'as' => 'edit']);
            Route::delete('{id}',           ['uses' => 'Admin\CategoryController@destroy',  'as' => 'destroy']);
            Route::put('{id}',              ['uses' => 'Admin\CategoryController@update',   'as' => 'update']);
        });

        // Rota Clientes

        Route::group(['prefix' => 'user','as' => 'user.'],function() {
            Route::get('/',             ['uses' => 'Admin\UserController@index',    'as' => 'index']);
            Route::post('store',        ['uses' => 'Admin\UserController@store',    'as' => 'store']);
            Route::get('/{id}/edit',    ['uses' => 'Admin\UserController@edit',     'as' => 'edit']);
            Route::get('create',        ['uses' => 'Admin\UserController@create',   'as' => 'create']);
            Route::get('export',        ['uses' => 'Admin\UserController@export',   'as' => 'export']);
        });

        // Rota Pedidos

        Route::group(['prefix' => 'order','as' => 'order.'],function(){

            Route::get('/',                 ['uses' => 'Admin\OrderController@index',                   'as' => 'index']);
            Route::get('/create',           ['uses' => 'Admin\OrderController@create',                  'as' => 'create']);
            Route::post('/check',           ['uses' => 'Admin\OrderController@check',                   'as' => 'check']);
            Route::get('/{id}',             ['uses' => 'Admin\OrderController@show',                    'as' => 'show']);
            Route::put('/{id}',             ['uses' => 'Admin\OrderController@update',                  'as' => 'update']);
            Route::get('/{id}/{status}',    ['uses' => 'Admin\OrderController@update_order_status',     'as' => 'update_order_status']);
        });

        // Rota Entrega

        Route::group(['prefix' => 'delivery','as' => 'delivery.'],function(){

            Route::get('/',                 ['uses' => 'Admin\DeliveryController@index', 'as' => 'index']);
            Route::post('/',                ['uses' => 'Admin\DeliveryController@store', 'as' => 'store']);
        });

        // Rota Atendentes

        Route::group(['prefix' => 'admin','as' => 'admin.'],function(){

            Route::get('/',                 ['uses' => 'Admin\AdminController@index',       'as' => 'index']);
            Route::post('/',                ['uses' => 'Admin\AdminController@store',       'as' => 'store']);
            Route::get('create',            ['uses' => 'Admin\AdminController@create',      'as' => 'create']);
            Route::get('{id}/edit',         ['uses' => 'Admin\AdminController@edit',        'as' => 'edit']);
            Route::put('update/{id}',       ['uses' => 'Admin\AdminController@update',      'as' => 'update']);
            Route::delete('{id}',           ['uses' => 'Admin\AdminController@destroy',     'as' => 'destroy']);

        });
    });
});